#ifndef __HDR_IMPORT_QT_LIBS__
#define __HDR_IMPORT_QT_LIBS__

/*!
    QT 에서 정적 플러그인을 불러오는 방법
    
    #. Q_IMPORT_PLUGIN 매크로를 사용하여 사용할 플러그인을 불러온다.
    #. #pragma comment 등을 사용하여 해당 플러그인 라이브러리를 가져온다. 

    SQLDrivers 

        QODBCDriverPlugin, qsqlodbc, qsqlodbcd, QT_STATIC_USE_ODBC_PLUGIN
        QSQLiteDriverPlugin, qsqlite, qsqlited, QT_STATIC_USE_SQLITE_PLUGIN
        QOCIDriverPlugin, qsqloci, qsqlocid, QT_STATIC_USE_OCI_PLUGIN
        QMYSQLDriverPlugin, qsqlmysql, qsqlmysqld, QT_STATIC_USE_MYSQL_PLUGIN

    Codecs ( Only Qt 4 )
        KRTextCodecs, qkrcodecs, qkrcodecsd, QT_USE_CODEC_KR
        CNTextCodecs, qcncodecs, qcncodecsd, QT_USE_CODEC_CN
        JPTextCodecs, qjpcodecs, qjpcodecsd, QT_USE_CODEC_JP
        TWTextCodecs, qtwcodecs, qtwcodecsd, QT_USE_CODEC_TW

    ImageFormats

        QICOPlugin qico QT_STATIC_USE_ICO_PLUGIN
        QGifPlugin 
        QJpegPlugin 
        QMngPlugin
        QSvgPlugin qsvg QT_STATIC_USE_SVG_PLUGIN
        QTgaPlugin
        QTiffPlugin

*/
#define QT_PREFIX "Qt"

#if defined(_DEBUG)
#define QT_DEBUG_LIB "d"
#else
#define QT_DEBUG_LIB ""
#endif

#define QT_SUFFIX ".lib"

//////////////////////////////////////////////////////////////////////////

#if QT_VERSION <= QT_VERSION_CHECK(4, 8, 7)
    #define QT_MAJOR_VER ""
#elif (QT_VERSION >= QT_VERSION_CHECK(4, 9, 0)) && (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    #define QT_MAJOR_VER "4"
#elif QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
    #define QT_MAJOR_VER "5"
#endif

//////////////////////////////////////////////////////////////////////////
/// 공통 

#pragma comment( lib, "qtmain" QT_DEBUG_LIB QT_SUFFIX )

#if defined(QT_CORE_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Core" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_OPENGL_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "OpenGL" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_GUI_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Gui" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_WIDGETS_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Widgets" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_SQL_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Sql" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_NETWORK_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Network" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_TESTLIB_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Test" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_QML_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Qml" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_CONCURRENT_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Concurrent" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_UITOOLS_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "UiTools" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_SCRIPT_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Script" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_SCRIPTTOOLS_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "ScriptTools" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_QUICK_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Quick" QT_DEBUG_LIB QT_SUFFIX )
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "QuickWidgets" QT_DEBUG_LIB QT_SUFFIX )
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "QuickParticles" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_XML_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Xml" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_XMLPATTERNS_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "XmlPatterns" QT_DEBUG_LIB QT_SUFFIX )
#endif

//////////////////////////////////////////////////////////////////////////
/// 사용자 라이브러리

#if defined(QT_FTP_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Ftp" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_HTTP_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Http" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_COMPRESS_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Compress" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_REMOTEOBJECTS_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "RemoteObjects" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_QUAZIP_LIB)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "Quazip" QT_DEBUG_LIB QT_SUFFIX )
#endif

//////////////////////////////////////////////////////////////////////////
/// 특수 링크

#if QT_VERSION <= QT_VERSION_CHECK(4,8,7)
    #if defined(QT_USE_CODEC_KR)
        #pragma comment(lib, "q" "kr" "codecs" QT_DEBUG_LIB QT_SUFFIX )
    #endif
#endif

#if QT_VERSION >= QT_VERSION_CHECK(4,9,0)
    #if !defined(QT_NO_REGULAREXPRESSION)
        #pragma comment(lib, QT_PREFIX "pcre" QT_DEBUG_LIB QT_SUFFIX )
    #endif
#endif

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
    #pragma comment(lib, QT_PREFIX QT_MAJOR_VER "PlatformSupport" QT_DEBUG_LIB QT_SUFFIX )

    #if !defined(QT_SHARED) || defined(QT_STATIC)
        #pragma comment(lib, "qwindows" QT_DEBUG_LIB QT_SUFFIX )
    #endif
#endif

//////////////////////////////////////////////////////////////////////////
/// ImageFormat 플러그인

#if defined(QT_STATIC_USE_ICO_PLUGIN)
    #pragma comment(lib, "q" "ico" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_STATIC_USE_SVG_PLUGIN)
    #pragma comment(lib, "q" "svg" QT_DEBUG_LIB QT_SUFFIX )
#endif

//////////////////////////////////////////////////////////////////////////
/// SQL 플러그인

#if defined(QT_STATIC_USE_ODBC_PLUGIN)
    #pragma comment(lib, "qsql" "odbc" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_STATIC_USE_SQLITE_PLUGIN)
    #pragma comment(lib, "qsql" "ite" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_STATIC_USE_OCI_PLUGIN)
    #pragma comment(lib, "qsql" "oci" QT_DEBUG_LIB QT_SUFFIX )
#endif

#if defined(QT_STATIC_USE_MYSQL_PLUGIN)
    #pragma comment(lib, "qsql" "mysql" QT_DEBUG_LIB QT_SUFFIX )
#endif

#endif // __HDR_IMPORT_QT_LIBS__
