﻿#ifndef __HDR_CMN_DB__
#define __HDR_CMN_DB__

#include "cmnBase.hpp"

#include "cmnDB_Defs.hpp"
#include "cmnDB_Mgr.hpp"
#include "cmnDB_Transaction.hpp"

#include <QtSql>

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {

        }   // nsDetail

        class CCmnDB
        {
        public:
            CCmnDB( const QSqlDatabase& db );

            //////////////////////////////////////////////////////////////////////////
            /// 

            bool                                    IsOpen();
            void                                    Close();

            QSqlQuery                               Exec( const QString& sQuery );
            bool                                    Prepare( QSqlQuery& dbStmt, const QString& sQuery, const std::string& loggerName = "" );
            bool                                    ExecuteSQL( QSqlQuery& dbStmt, const std::string& loggerName = "" );
            QSqlError                               LastSQLError();

            operator QSqlDatabase();

            //////////////////////////////////////////////////////////////////////////
            /// Transaction

            bool                                    Transaction();
            bool                                    ImmediateTransaction();
            bool                                    Commit();
            bool                                    Rollback();

            bool                                    SavePoint( const QString& sSPName );
            bool                                    RollbackTo( const QString& sSPName );
            bool                                    ReleaseTo( const QString& sSPName );

            //////////////////////////////////////////////////////////////////////////
            /// 데이터 관리, 트랜잭션은 별도로 사용하지 않음, 외부에서 직접 트랜잭션 사용해야함

            QVariant                                GetValue( const QSqlQuery& dbStmt, const QString& name, const QVariant& vtDefault = QVariant() ) const;
            QVector< QString >                      GetColumns( QSqlRecord& record );
            QHash< QString, QVariant >              GetRecordData( QSqlRecord& record, const QVector< QString >& vecColumns );

            bool                                    SetTableData( const QString& tableName, const TyPrColumnNameToValue& prColNameToValue );
            // 테이블에서 columnName = columnData 인 조건에 만족하는 레코드에서 select 컬럼의 값을 반환한다. 
            QVariant                                GetTableData( const QString& tableName, const QString& select, const TyPrColumnNameToValue& prColNameToValue );
//             // 테이블에서 지정한 컬럼의 값 목록을 반환한다. 순서는 정해지지 않음. 
//             QVector< QVariant >                     GetTableDataSet( const std::string& dbIdentity, const QString& tableName, const QString& select );
//             bool                                    IsExistTableData( const std::string& dbIdentity, const QString& tableName, const QString& columnName, const QVariant& columnData );
            // INSERT OR REPLACE 구문을 이용하여 값을 추가함
            bool                                    InsertTableKVPairs( const QString& tableName, const QHash< QString, QVariant >& mapColNameToValue );
//             //             QVariant                                GetTableKVData( const std::string& dbIdentity, const QString& tableName, const QString& columnName, const QString& key );
//             //             bool                                    IsExistTableKVData( const std::string& dbIdentity, const QString& tableName, const QString& columnName, const QString& key, const QVariant& value );
            bool                                    DeleteTable( const QString& tableName );

            //////////////////////////////////////////////////////////////////////////
            ///

            QString                                 GetDBIdentity();
            void                                    SetDBIdentity( const QString& sDBIdentity );

            QString                                 GetDBType();
            void                                    SetDBType( const QString& sDBType );

            QString                                 GetDBConnectionText();
            void                                    SetDBConnectionText( const QString& sDBConnection );

            //////////////////////////////////////////////////////////////////////////
            ///

            std::pair< int, std::wstring >          GetNativeErrorInfo();

        private:

            QString                                 _dbIdentity;
            QString                                 _dbType;
            QString                                 _dbConnection;

            QSqlDatabase                            _db;
        };

    } // nsCmnDB
} // nsCmn

#endif // __HDR_CMN_DB__