﻿#ifndef __HDR_CMN_BASE__
#define __HDR_CMN_BASE__

#include <cassert>

#include "cmnSystemDetection.hpp"
#include "cmnCompilerDetection.hpp"

#define IF_FALSE_BREAK( var, expr ) if( ( (var) = (expr) ) == false ) break;
#define IF_SUCCESS_BREAK( var, expr ) if( ( (var) = (expr) ) == true ) break;

#include "cmnTypes.hpp"

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {

        }   // nsDetail
    } // nsCmnDB
} // nsCmn

#endif // __HDR_CMN_BASE__