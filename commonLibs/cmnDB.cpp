﻿#include "stdafx.h"
#include "cmnDB.hpp"

#include <QtCore>

#include "sqlite3.h"

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {
            QThreadStorage< TyStkTransaction > G_TransactionStk;
        }   // nsDetail

        CCmnDB::CCmnDB( const QSqlDatabase& db )
            : _db( db )
        {
            
        }

        bool CCmnDB::IsOpen()
        {
            return _db.isOpen();
        }

        void CCmnDB::Close()
        {
            _db.close();
        }

        QSqlQuery CCmnDB::Exec( const QString& sQuery )
        {
            return _db.exec( sQuery );
        }

        bool CCmnDB::Prepare( QSqlQuery& dbStmt, const QString& sQuery, const std::string& loggerName /* = "" */ )
        {
            bool isSuccess = false;
            bool isTryAgain = false;

            do
            {
                isTryAgain = false;

                if( (isSuccess = dbStmt.prepare( sQuery )) == false )
                {
                    QSqlError lastError = dbStmt.lastError();
//                     LM_ERROR_TO( loggerName, L"SQL 문장 {} 를 준비하는 동안 오류가 발생하였습니다, 오류 = {}, {}",
//                                  sqlQuery, lastError.text(), lastError.number() );
// 
                    QVariant vtDriver = dbStmt.driver()->handle();
                    if( vtDriver.isValid() && qstricmp( vtDriver.typeName(), "sqlite3*" ) == 0 )
                    {
                        int errcode = lastError.number();
                        // number = 5 , SQLITE_BUSY
                        // number = 6 , SQLITE_LOCKED
                        if( errcode == 5 || errcode == 6 )
                        {
                            isTryAgain = true;
                            QThread::msleep( 5 );
                        }
                    }
                }

            } while( isTryAgain == true );

            return isSuccess;
        }

        bool CCmnDB::ExecuteSQL( QSqlQuery& dbStmt, const std::string& loggerName /*= "" */ )
        {
            bool isSuccess = false;
            bool isTryAgain = false;

            do
            {
                isTryAgain = false;

                if( (isSuccess = dbStmt.exec()) == false )
                {
                    QSqlError lastError = dbStmt.lastError();
                    //                     LM_ERROR_TO( loggerName, L"SQL 문장 {} 를 준비하는 동안 오류가 발생하였습니다, 오류 = {}, {}",
                    //                                  sqlQuery, lastError.text(), lastError.number() );
                    // 
                    QVariant vtDriver = dbStmt.driver()->handle();
                    if( vtDriver.isValid() && qstricmp( vtDriver.typeName(), "sqlite3*" ) == 0 )
                    {
                        int errcode = lastError.number();
                        // number = 5 , SQLITE_BUSY
                        // number = 6 , SQLITE_LOCKED
                        if( errcode == 5 || errcode == 6 )
                        {
                            isTryAgain = true;
                            QThread::msleep( 5 );
                        }
                    }
                }

            } while( isTryAgain == true );

            return isSuccess;
        }

        QSqlError CCmnDB::LastSQLError()
        {
            return _db.lastError();
        }

        CCmnDB::operator QSqlDatabase()
        {
            return _db;
        }

        bool CCmnDB::Transaction()
        {
            bool isSuccess = _db.transaction();
            if( isSuccess == true )
                nsDetail::G_TransactionStk.localData().push( "BEGIN" );

            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|{}|{:b}|{}", GetDBConnection(), "BEGIN", isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::ImmediateTransaction()
        {
            bool isSuccess = false;
            QSqlQuery dbStmt = _db.exec( "BEGIN IMMEDIATE" );
            if( dbStmt.lastError().type() == QSqlError::NoError )
            {
                nsDetail::G_TransactionStk.localData().push( "BEGIN IMMEDIATE" );
                isSuccess = true;
            }

            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|{}|{:b}|{}", GetDBConnection(), "BEGIN IMMEDIATE", isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::Commit()
        {
            bool isSuccess = _db.commit();
            if( isSuccess == true )
                nsDetail::G_TransactionStk.localData().pop();

            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|{}|{:b}|{}", GetDBConnection(), "COMMIT", isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::Rollback()
        {
            bool isSuccess = _db.rollback();
            if( isSuccess == true )
                nsDetail::G_TransactionStk.localData().pop();

            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|{}|{:b}|{}", GetDBConnection(), "ROLLBACK", isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::SavePoint( const QString& sSPName )
        {
            bool isSuccess = false;
            QSqlQuery dbStmt = _db.exec( QString( "SAVEPOINT '%1'" ).arg( sSPName ) );
            if( dbStmt.lastError().type() == QSqlError::NoError )
            {
                nsDetail::G_TransactionStk.localData().push( sSPName );
                isSuccess = true;
            }

            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|SAVEPOINT {}|{:b}|{}", GetDBConnection(), sSPName, isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::RollbackTo( const QString& sSPName )
        {
            bool isSuccess = false;
            QSqlQuery dbStmt = _db.exec( QString( "ROLLBACK TO '%1'" ).arg( sSPName ) );
            if( dbStmt.lastError().type() == QSqlError::NoError )
                isSuccess = true;

            /// ROLLBACK TO 의 경우에는 트랜잭션 스택에 관여하지 않음
            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|ROLLBACK TO {}|{:b}|{}", GetDBConnection(), sSPName, isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::ReleaseTo( const QString& sSPName )
        {
            bool isSuccess = false;
            QSqlQuery dbStmt = _db.exec( QString( "RELEASE '%1'" ).arg( sSPName ) );
            if( dbStmt.lastError().type() == QSqlError::NoError )
            {
                nsCmn::nsCmnDB::nsDetail::TyStkTransaction& stkTransaction = nsDetail::G_TransactionStk.localData();

                if( sSPName.compare( stkTransaction.top() ) != 0 )
                {
                    Q_ASSERT_X( false, __FUNCTION__, "트랜잭션 스택의 이름이 다름" );
                }

                stkTransaction.pop();
                isSuccess = true;
            }

            /// 스레드 ID, 연결이름, 트랜잭션 종류, 트랜잭션 결과, 오류
//             LM_LOG_TO( isSuccess == true ? nsCmn::nsCmnLogger::LL_TRACE : nsCmn::nsCmnLogger::LL_ERROR, logger_dbtransaction, L"{TID}|{}|RELEASE {}|{:b}|{}", GetDBConnection(), sSPName, isSuccess, isSuccess == true ? std::make_pair( 0, std::wstring() ) : GetNativeErrorInfo() );
            return isSuccess;
        }

        bool CCmnDB::InsertTableKVPairs( const QString& tableName, const QHash< QString, QVariant >& mapColNameToValue )
        {
            bool isSuccess = false;

            do
            {
                if( mapColNameToValue.isEmpty() == true )
                    break;

                QSqlQuery dbStmt( _db );

                QString sQuery = QString( "INSERT OR REPLACE INTO %1 ( " ).arg( tableName );
                QStringList lst = mapColNameToValue.keys();

                sQuery += lst.join( "," );
                sQuery += " ) VALUES ( ";

                lst = lst.replaceInStrings( QRegExp( "^(.*)$" ), QString( ":\\1" ) );

                sQuery += lst.join( "," );
                sQuery += " ) ";

                IF_FALSE_BREAK( isSuccess, Prepare( dbStmt, sQuery ) );
                for( QHash< QString, QVariant >::const_iterator it = mapColNameToValue.begin(); it != mapColNameToValue.end(); ++it )
                    dbStmt.bindValue( ":" + it.key(), it.value() );
                IF_FALSE_BREAK( isSuccess, ExecuteSQL( dbStmt ) );

            } while( false );

            return isSuccess;
        }

        bool CCmnDB::DeleteTable( const QString& tableName )
        {
            bool isSuccess = false;

            do 
            {
                QSqlQuery dbStmt( _db );

                QString sQuery = QString("DROP TABLE IF EXISTS %1").arg( tableName );
                IF_FALSE_BREAK( isSuccess, Prepare( dbStmt, sQuery ) );
                IF_FALSE_BREAK( isSuccess, ExecuteSQL( dbStmt ) );

            } while (false);

            return isSuccess;
        }

        QString CCmnDB::GetDBIdentity()
        {
            return _dbIdentity;
        }

        void CCmnDB::SetDBIdentity( const QString& sDBIdentity )
        {
            _dbIdentity = sDBIdentity;
        }

        QString CCmnDB::GetDBType()
        {
            return _dbType;
        }

        void CCmnDB::SetDBType( const QString& sDBType )
        {
            _dbType = sDBType;
        }

        QString CCmnDB::GetDBConnectionText()
        {
            return _dbConnection;
        }

        void CCmnDB::SetDBConnectionText( const QString& sDBConnection )
        {
            _dbConnection = sDBConnection;
        }

        //////////////////////////////////////////////////////////////////////////

        std::pair< int, std::wstring > CCmnDB::GetNativeErrorInfo()
        {
            std::pair< int, std::wstring > prErrorInfo( 0, L"" );

            do
            {
                if( _db.isOpen() == false || _db.isValid() == false )
                    break;

                if( GetDBType().compare( "QSQLITE" ) == 0 )
                {
                    QSqlDriver* driver = _db.driver();
                    Q_CHECK_PTR( driver );
                    QVariant v = driver->handle();
                    if( v.isValid() == false || qstrcmp( v.typeName(), "sqlite3*" ) != 0 )
                        break;

                    sqlite3* handle = *static_cast<sqlite3 **>(v.data());
                    if( handle == NULLPTR )
                        break;

                    prErrorInfo.first = sqlite3_errcode( handle );
                    prErrorInfo.second = reinterpret_cast<const wchar_t*>(sqlite3_errmsg16( handle ));
                }

            } while( false );

            return prErrorInfo;
        }

        //////////////////////////////////////////////////////////////////////////
        /// Transaction Mgr
        //////////////////////////////////////////////////////////////////////////


        CCmnDBBasicTransaction::CCmnDBBasicTransaction( TySpCmnDB connection )
            : _connection( connection )
        {}

        CCmnDBBasicTransaction::~CCmnDBBasicTransaction()
        {}

        bool CCmnDBBasicTransaction::begin()
        {
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;
            bool isSuccess = false;

            do
            {
                isSuccess = _connection->Transaction();
                _lasterror = _connection->LastSQLError();
                if( isSuccess == false )
                {
                    prNativeErrorInfo = _connection->GetNativeErrorInfo();
                    if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                        prNativeErrorInfo.first == SQLITE_BUSY )
                    {
//                         LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|BEGIN|{}", ++transactionCount );
                        QThread::msleep( 300 );
                        continue;
                    }

                    // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                    break;
                }

            } while( isSuccess == false );

            return isSuccess;
        }

        bool CCmnDBBasicTransaction::commit()
        {
            // we must reset any active queries when rolling back
            reset_active_queries();
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;
            bool isSuccess = false;

            do
            {
                isSuccess = _connection->Commit();
                _lasterror = _connection->LastSQLError();
                if( isSuccess == false )
                {
                    prNativeErrorInfo = _connection->GetNativeErrorInfo();
                    if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                        prNativeErrorInfo.first == SQLITE_BUSY )
                    {
//                         LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|COMMIT|{}", ++transactionCount );
                        QThread::msleep( 300 );
                        continue;
                    }

                    // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                    break;
                }

            } while( isSuccess == false );

            return isSuccess;
        }

        void CCmnDBBasicTransaction::reset_active_queries()
        {
            // TODO: SQLITE3 일 경우에만 아래 내용을 수행하도록 코드 작성
            //             if( nsCmn::stricmp( _connection->GetDBType(), "SQLITE" ) == 0 )
            //             {
            //                 sqlite3_stmt *old_handle = NULL;
            //                 while (sqlite3_stmt *handle =
            //                     sqlite3_next_stmt(_connection._handle, old_handle))
            //                 {
            //                     int code = sqlite3_reset(handle);
            //                     if (code != SQLITE_OK) throw sqlite_error(_connection, code);
            //                     old_handle = handle;
            //                 }
            //             }
        }

        bool CCmnDBBasicTransaction::rollback()
        {
            // we must reset any active queries when rolling back
            reset_active_queries();
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;
            bool isSuccess = false;

            do
            {
                isSuccess = _connection->Rollback();
                _lasterror = _connection->LastSQLError();
                if( isSuccess == false )
                {
                    prNativeErrorInfo = _connection->GetNativeErrorInfo();
                    if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                        prNativeErrorInfo.first == SQLITE_BUSY )
                    {
//                         LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|ROLLBACK|{}", ++transactionCount );
                        QThread::msleep( 300 );
                        continue;
                    }

                    // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                    break;
                }

            } while( isSuccess == false );

            return isSuccess;
        }

#if defined( USE_QT_SUPPORT ) && defined( QT_QTSQL_MODULE_H )
        QSqlError CCmnDBBasicTransaction::getLastError()
        {
            return _lasterror;
        }
#endif

        CCmnDBImmediateTransaction::CCmnDBImmediateTransaction( TySpCmnDB connection )
            : CCmnDBBasicTransaction( connection )
        {}


        CCmnDBImmediateTransaction::~CCmnDBImmediateTransaction()
        {

        }

        bool CCmnDBImmediateTransaction::begin()
        {
            // CCmnDBImmediateTransaction 중첩 트랜잭션의 중간에 끼일 수 없다.
            assert( nsDetail::G_TransactionStk.localData().empty() == true );
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;
            bool isSuccess = false;

            do
            {
                isSuccess = _connection->ImmediateTransaction();
                _lasterror = _connection->LastSQLError();
                if( isSuccess == false )
                {
                    prNativeErrorInfo = _connection->GetNativeErrorInfo();
                    if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                        prNativeErrorInfo.first == SQLITE_BUSY )
                    {
//                         LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|BEGIN IMMEDIATE|{}", ++transactionCount );
                        QThread::msleep( 300 );
                        continue;
                    }

                    // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                    break;
                }

            } while( isSuccess == false );

            return isSuccess;
        }

        Q_GLOBAL_STATIC_WITH_ARGS( QAtomicInt, globalRecursiveNo, (0) );

        CCmnDBRecursiveTransaction::CCmnDBRecursiveTransaction( TySpCmnDB connection )
            : CCmnDBImmediateTransaction( connection )
            , _isInitialEmptyStack( false )
            , _isInitialTransaction( false )
        {
            // fetchAndAddOrdered 메소드는 기존 값에 값을 더한 후 기존 값을 반환한다. 
            int no = globalRecursiveNo->fetchAndAddOrdered( 1 ) + 1;
            _sp_name = QString( "%1_SP_%2_%3" ).arg( ::GetCurrentThreadId() ).arg( _connection->GetDBConnectionText(), no );
        }

        CCmnDBRecursiveTransaction::~CCmnDBRecursiveTransaction()
        {}

        bool CCmnDBRecursiveTransaction::begin()
        {
            _isInitialTransaction = false;
            _isInitialEmptyStack = false;
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;

            do
            {
                nsCmn::nsCmnDB::nsDetail::TyStkTransaction& stkTransaction = nsDetail::G_TransactionStk.localData();

                // 해당 스레드의 트랜잭션 스택이 비어있다면 ImmediateTransaction 을 획득하도록 한다. 
                _isInitialEmptyStack = stkTransaction.empty();
                if( _isInitialEmptyStack == true )
                {
                    // SQLITE_LOCKED, 또는 SQLITE_BUSY 일 경우에는 잠금을 재시도한다. 
                    _isInitialTransaction = _connection->ImmediateTransaction();
                    _lasterror = _connection->LastSQLError();;
                    if( _isInitialTransaction == false )
                    {
                        prNativeErrorInfo = _connection->GetNativeErrorInfo();
                        if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                            prNativeErrorInfo.first == SQLITE_BUSY )
                        {
//                             LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|{}|{}", _sp_name, ++transactionCount );
                            QThread::msleep( 300 );
                            continue;
                        }

                        // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                        break;
                    }
                }

                do
                {
                    _isInitialTransaction = _connection->SavePoint( _sp_name );
                    _lasterror = _connection->LastSQLError();;
                    if( _isInitialTransaction == false )
                    {
                        prNativeErrorInfo = _connection->GetNativeErrorInfo();

                        if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                            prNativeErrorInfo.first == SQLITE_BUSY )
                        {
//                             LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|{}|{}", _sp_name, ++transactionCount );
                            QThread::msleep( 300 );
                            continue;
                        }

                        // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                        Q_ASSERT( false );
                        break;
                    }

                } while( _isInitialTransaction == false );

                if( _isInitialTransaction == false &&
                    _isInitialEmptyStack == true )
                {
                    // rollback 에 대한 로그는 내부에서 출력된다. while 문을 벗어나기 위해 break 호출( break 를 호출하지 않으면 잠금을 재시도하게 된다 )
                    // 트랜잭션을 설정하는데 실패하였을 때 _isInitialTransaction 이 false 상태로 유지될 수 있도록 한다. 
                    CCmnDBImmediateTransaction::rollback();
                    break;
                }

            } while( _isInitialTransaction == false && _isInitialEmptyStack == true );

            return _isInitialTransaction;
        }

        bool CCmnDBRecursiveTransaction::commit()
        {
            // we must reset any active queries when committing
            bool isSuccess = false;
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;

            do
            {
                assert( _isInitialTransaction == true );
                if( _isInitialTransaction == false )
                {
                    isSuccess = true;
                    break;
                }

                nsCmn::nsCmnDB::nsDetail::TyStkTransaction& stkTransaction = nsDetail::G_TransactionStk.localData();

                assert( stkTransaction.empty() == false );
                QString topSpName = stkTransaction.top();
                assert( topSpName == _sp_name );
                if( _sp_name != topSpName  )
                {
//                     LM_ERROR_TO( cmndbmanager_logger, L"잘못된 트랜잭션 사용, 트랜잭션 스택 쌍이 맞지 않습니다|{}|{}", _sp_name, topSpName );
                    break;
                }

                reset_active_queries();

                do
                {
                    isSuccess = _connection->ReleaseTo( _sp_name );
                    _lasterror = _connection->LastSQLError();;
                    if( isSuccess == false )
                    {
                        prNativeErrorInfo = _connection->GetNativeErrorInfo();

                        if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                            prNativeErrorInfo.first == SQLITE_BUSY )
                        {
//                             LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|{}|{}", _sp_name, ++transactionCount );
                            QThread::msleep( 300 );
                            continue;
                        }

                        // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                        Q_ASSERT( false );
                        break;
                    }

                } while( isSuccess == false );

                // Recursive 에서 직접 BEGIN 을 했다면 이곳에서 commit 까지 호출해준다. 
                if( _isInitialEmptyStack == true &&
                    stkTransaction.size() == 1 )
                {
                    IF_FALSE_BREAK( isSuccess, CCmnDBImmediateTransaction::commit() );
                }

                isSuccess = true;
                break;

            } while( false );

            return isSuccess;
        }

        bool CCmnDBRecursiveTransaction::rollback()
        {
            bool isSuccess = false;
            std::pair< int, std::wstring > prNativeErrorInfo;
            int transactionCount = 0;

            do
            {
                assert( _isInitialTransaction == true );
                if( _isInitialTransaction == false )
                {
                    isSuccess = true;
                    break;
                }

                nsCmn::nsCmnDB::nsDetail::TyStkTransaction& stkTransaction = nsDetail::G_TransactionStk.localData();

                assert( stkTransaction.empty() == false );
                QString topSpName = stkTransaction.top();
                assert( topSpName == _sp_name );
                if( _sp_name != topSpName  )
                {
//                     LM_ERROR_TO( cmndbmanager_logger, L"잘못된 트랜잭션 사용, 트랜잭션 스택 쌍이 맞지 않습니다|{}|{}", _sp_name, topSpName );
                    break;
                }

                // we must reset any active queries when rolling back
                reset_active_queries();

                do
                {
                    isSuccess = _connection->RollbackTo( _sp_name );
                    _lasterror = _connection->LastSQLError();

                    if( isSuccess == false )
                    {
                        prNativeErrorInfo = _connection->GetNativeErrorInfo();

                        if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                            prNativeErrorInfo.first == SQLITE_BUSY )
                        {
//                             LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|{}|{}", _sp_name, ++transactionCount );
                            QThread::msleep( 300 );
                            continue;
                        }

                        // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                        Q_ASSERT( false );
                        break;
                    }

                } while( isSuccess == false );

                if( isSuccess == false )
                    break;

                // we have rolled back this transaction's savepoint, but the savepoint will
                // remain on the transaction stack unless we also release it

                do
                {
                    isSuccess = _connection->ReleaseTo( _sp_name );
                    _lasterror = _connection->LastSQLError();

                    if( isSuccess == false )
                    {
                        prNativeErrorInfo = _connection->GetNativeErrorInfo();

                        if( prNativeErrorInfo.first == SQLITE_LOCKED ||
                            prNativeErrorInfo.first == SQLITE_BUSY )
                        {
//                             LM_ERROR_TO( logger_dbtransaction, L"트랜잭션 잠금 재시도|{}|{}", _sp_name, ++transactionCount );
                            QThread::msleep( 300 );
                            continue;
                        }

                        // SQLITE_LOCKED 또는 SQLITE_BUSY 외의 다른 오류는 오류를 반환하도록 한다.
                        Q_ASSERT( false );
                        break;
                    }

                } while( isSuccess == false );

                // Recursive 에서 직접 BEGIN 을 했다면 이곳에서 rollback 까지 호출해준다. 
                if( _isInitialEmptyStack == true &&
                    stkTransaction.size() == 1 )
                {
                    IF_FALSE_BREAK( isSuccess, CCmnDBImmediateTransaction::rollback() );
                }

                isSuccess = true;
                break;

            } while( false );

            return isSuccess;
        }

    } // nsCmnDB
} // nsCmn
