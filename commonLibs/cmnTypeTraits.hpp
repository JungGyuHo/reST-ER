﻿#ifndef __HDR_CMN_TRAITS__
#define __HDR_CMN_TRAITS__

#include "cmnBase.hpp"

#include <functional>
#include <type_traits>

#if defined(_MSC_VER)
#   pragma execution_character_set( "utf-8" )
#endif

//////////////////////////////////////////////////////////////////////////

namespace nsCmn
{
    namespace nsTraits
    {
        //////////////////////////////////////////////////////////////////////////
        /// integral_constant

        template< class T, T val >
        struct integral_constant
        {
            static const T value = val;

            typedef T value_type;
            typedef integral_constant<T, val> type;
        };

        typedef integral_constant<bool, true>       true_type;
        typedef integral_constant<bool, false>      false_type;

        //////////////////////////////////////////////////////////////////////////
        /// remove_reference

        template< class T > struct remove_reference { typedef T type; };
        template< class T > struct remove_reference<T&> { typedef T type; };
#if defined(CMN_USE_RVALUE_REFERENCES)
        template< class T > struct remove_reference<T&&> { typedef T type; };
#endif

// C++14
//         template< class T >
//         using remove_reference_t = typename remove_reference<T>::type;

        template< class T > struct remove_const { typedef T type; };
        template< class T > struct remove_const<const T> { typedef T type; };

        template< class T > struct remove_volatile { typedef T type; };
        template< class T > struct remove_volatile<volatile T> { typedef T type; };

        template< class T > struct remove_cv { typedef typename remove_volatile<typename remove_const<T>::type>::type type; };

        //////////////////////////////////////////////////////////////////////////
        /// is_pointer

        template< class T > struct is_pointer_helper : false_type {};
        template< class T > struct is_pointer_helper<T*> : true_type {};
        template< class T > struct is_pointer : is_pointer_helper<typename remove_cv<T>::type> {};


        template<typename T, typename R = void>
        struct enable_if_has_type
        {
            typedef R type;
        };

        template <bool B, class T = void>
        struct enable_if_c
        {
            typedef T type;
        };

        template <class T>
        struct enable_if_c<false, T> {};

        template <class Cond, class T = void>
        struct enable_if : public enable_if_c<Cond::value, T> {};

        template <bool B, class T = void>
        struct disable_if_c
        {
            typedef T type;
        };

        template <class T>
        struct disable_if_c<true, T> {};

        template <class Cond, class T = void>
        struct disable_if : public disable_if_c<Cond::value, T> {};

        //////////////////////////////////////////////////////////////////////////
        /// is_same

        template <class T, class U> struct is_same : public false_type {};
        template <class T> struct is_same<T, T> : public true_type {};

    } // nsTraits
} // nsCmn

#endif // __HDR_CMN_TRAITS__
