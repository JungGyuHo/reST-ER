// Copyright 2008 Alexandre Courpron                                        
//                                                                          
// This file is part of the tek library.
// The tek library is licensed under the Code Project Open License v1.0.


#ifndef TEK_PREPROCESSOR_INC_H
#define TEK_PREPROCESSOR_INC_H

#define TEK_PP_INC0 1
#define TEK_PP_INC1 2
#define TEK_PP_INC2 3
#define TEK_PP_INC3 4
#define TEK_PP_INC4 5
#define TEK_PP_INC5 6
#define TEK_PP_INC6 7
#define TEK_PP_INC7 8
#define TEK_PP_INC8 9
#define TEK_PP_INC9 10
#define TEK_PP_INC10 11
#define TEK_PP_INC11 12
#define TEK_PP_INC12 13
#define TEK_PP_INC13 14
#define TEK_PP_INC14 15
#define TEK_PP_INC15 16
#define TEK_PP_INC16 17
#define TEK_PP_INC17 18
#define TEK_PP_INC18 19
#define TEK_PP_INC19 20
#define TEK_PP_INC20 21
#define TEK_PP_INC21 22
#define TEK_PP_INC22 23
#define TEK_PP_INC23 24
#define TEK_PP_INC24 25
#define TEK_PP_INC25 26
#define TEK_PP_INC26 27
#define TEK_PP_INC27 28
#define TEK_PP_INC28 29
#define TEK_PP_INC29 30
#define TEK_PP_INC30 31
#define TEK_PP_INC31 32
#define TEK_PP_INC32 33
#define TEK_PP_INC33 34
#define TEK_PP_INC34 35
#define TEK_PP_INC35 36
#define TEK_PP_INC36 37
#define TEK_PP_INC37 38
#define TEK_PP_INC38 39
#define TEK_PP_INC39 40
#define TEK_PP_INC40 41
#define TEK_PP_INC41 42
#define TEK_PP_INC42 43
#define TEK_PP_INC43 44
#define TEK_PP_INC44 45
#define TEK_PP_INC45 46
#define TEK_PP_INC46 47
#define TEK_PP_INC47 48
#define TEK_PP_INC48 49
#define TEK_PP_INC49 50
#define TEK_PP_INC50 51
#define TEK_PP_INC51 52
#define TEK_PP_INC52 53
#define TEK_PP_INC53 54
#define TEK_PP_INC54 55
#define TEK_PP_INC55 56
#define TEK_PP_INC56 57
#define TEK_PP_INC57 58
#define TEK_PP_INC58 59
#define TEK_PP_INC59 60
#define TEK_PP_INC60 61
#define TEK_PP_INC61 62
#define TEK_PP_INC62 63
#define TEK_PP_INC63 64
#define TEK_PP_INC64 65
#define TEK_PP_INC65 66
#define TEK_PP_INC66 67
#define TEK_PP_INC67 68
#define TEK_PP_INC68 69
#define TEK_PP_INC69 70
#define TEK_PP_INC70 71
#define TEK_PP_INC71 72
#define TEK_PP_INC72 73
#define TEK_PP_INC73 74
#define TEK_PP_INC74 75
#define TEK_PP_INC75 76
#define TEK_PP_INC76 77
#define TEK_PP_INC77 78
#define TEK_PP_INC78 79
#define TEK_PP_INC79 80
#define TEK_PP_INC80 81
#define TEK_PP_INC81 82
#define TEK_PP_INC82 83
#define TEK_PP_INC83 84
#define TEK_PP_INC84 85
#define TEK_PP_INC85 86
#define TEK_PP_INC86 87
#define TEK_PP_INC87 88
#define TEK_PP_INC88 89
#define TEK_PP_INC89 90
#define TEK_PP_INC90 91
#define TEK_PP_INC91 92
#define TEK_PP_INC92 93
#define TEK_PP_INC93 94
#define TEK_PP_INC94 95
#define TEK_PP_INC95 96
#define TEK_PP_INC96 97
#define TEK_PP_INC97 98
#define TEK_PP_INC98 99
#define TEK_PP_INC99 100
#define TEK_PP_INC100 101
#define TEK_PP_INC101 102
#define TEK_PP_INC102 103
#define TEK_PP_INC103 104
#define TEK_PP_INC104 105
#define TEK_PP_INC105 106
#define TEK_PP_INC106 107
#define TEK_PP_INC107 108
#define TEK_PP_INC108 109
#define TEK_PP_INC109 110
#define TEK_PP_INC110 111
#define TEK_PP_INC111 112
#define TEK_PP_INC112 113
#define TEK_PP_INC113 114
#define TEK_PP_INC114 115
#define TEK_PP_INC115 116
#define TEK_PP_INC116 117
#define TEK_PP_INC117 118
#define TEK_PP_INC118 119
#define TEK_PP_INC119 120
#define TEK_PP_INC120 121
#define TEK_PP_INC121 122
#define TEK_PP_INC122 123
#define TEK_PP_INC123 124
#define TEK_PP_INC124 125
#define TEK_PP_INC125 126
#define TEK_PP_INC126 127
#define TEK_PP_INC127 128
#define TEK_PP_INC128 129
#define TEK_PP_INC129 130
#define TEK_PP_INC130 131
#define TEK_PP_INC131 132
#define TEK_PP_INC132 133
#define TEK_PP_INC133 134
#define TEK_PP_INC134 135
#define TEK_PP_INC135 136
#define TEK_PP_INC136 137
#define TEK_PP_INC137 138
#define TEK_PP_INC138 139
#define TEK_PP_INC139 140
#define TEK_PP_INC140 141
#define TEK_PP_INC141 142
#define TEK_PP_INC142 143
#define TEK_PP_INC143 144
#define TEK_PP_INC144 145
#define TEK_PP_INC145 146
#define TEK_PP_INC146 147
#define TEK_PP_INC147 148
#define TEK_PP_INC148 149
#define TEK_PP_INC149 150
#define TEK_PP_INC150 151
#define TEK_PP_INC151 152
#define TEK_PP_INC152 153
#define TEK_PP_INC153 154
#define TEK_PP_INC154 155
#define TEK_PP_INC155 156
#define TEK_PP_INC156 157
#define TEK_PP_INC157 158
#define TEK_PP_INC158 159
#define TEK_PP_INC159 160
#define TEK_PP_INC160 161
#define TEK_PP_INC161 162
#define TEK_PP_INC162 163
#define TEK_PP_INC163 164
#define TEK_PP_INC164 165
#define TEK_PP_INC165 166
#define TEK_PP_INC166 167
#define TEK_PP_INC167 168
#define TEK_PP_INC168 169
#define TEK_PP_INC169 170
#define TEK_PP_INC170 171
#define TEK_PP_INC171 172
#define TEK_PP_INC172 173
#define TEK_PP_INC173 174
#define TEK_PP_INC174 175
#define TEK_PP_INC175 176
#define TEK_PP_INC176 177
#define TEK_PP_INC177 178
#define TEK_PP_INC178 179
#define TEK_PP_INC179 180
#define TEK_PP_INC180 181
#define TEK_PP_INC181 182
#define TEK_PP_INC182 183
#define TEK_PP_INC183 184
#define TEK_PP_INC184 185
#define TEK_PP_INC185 186
#define TEK_PP_INC186 187
#define TEK_PP_INC187 188
#define TEK_PP_INC188 189
#define TEK_PP_INC189 190
#define TEK_PP_INC190 191
#define TEK_PP_INC191 192
#define TEK_PP_INC192 193
#define TEK_PP_INC193 194
#define TEK_PP_INC194 195
#define TEK_PP_INC195 196
#define TEK_PP_INC196 197
#define TEK_PP_INC197 198
#define TEK_PP_INC198 199
#define TEK_PP_INC199 200
#define TEK_PP_INC200 201
#define TEK_PP_INC201 202
#define TEK_PP_INC202 203
#define TEK_PP_INC203 204
#define TEK_PP_INC204 205
#define TEK_PP_INC205 206
#define TEK_PP_INC206 207
#define TEK_PP_INC207 208
#define TEK_PP_INC208 209
#define TEK_PP_INC209 210
#define TEK_PP_INC210 211
#define TEK_PP_INC211 212
#define TEK_PP_INC212 213
#define TEK_PP_INC213 214
#define TEK_PP_INC214 215
#define TEK_PP_INC215 216
#define TEK_PP_INC216 217
#define TEK_PP_INC217 218
#define TEK_PP_INC218 219
#define TEK_PP_INC219 220
#define TEK_PP_INC220 221
#define TEK_PP_INC221 222
#define TEK_PP_INC222 223
#define TEK_PP_INC223 224
#define TEK_PP_INC224 225
#define TEK_PP_INC225 226
#define TEK_PP_INC226 227
#define TEK_PP_INC227 228
#define TEK_PP_INC228 229
#define TEK_PP_INC229 230
#define TEK_PP_INC230 231
#define TEK_PP_INC231 232
#define TEK_PP_INC232 233
#define TEK_PP_INC233 234
#define TEK_PP_INC234 235
#define TEK_PP_INC235 236
#define TEK_PP_INC236 237
#define TEK_PP_INC237 238
#define TEK_PP_INC238 239
#define TEK_PP_INC239 240
#define TEK_PP_INC240 241
#define TEK_PP_INC241 242
#define TEK_PP_INC242 243
#define TEK_PP_INC243 244
#define TEK_PP_INC244 245
#define TEK_PP_INC245 246
#define TEK_PP_INC246 247
#define TEK_PP_INC247 248
#define TEK_PP_INC248 249
#define TEK_PP_INC249 250
#define TEK_PP_INC250 251
#define TEK_PP_INC251 252
#define TEK_PP_INC252 253
#define TEK_PP_INC253 254
#define TEK_PP_INC254 255
#define TEK_PP_INC255 256
#define TEK_PP_INC(x) TEK_PP_INC##x

#endif // #ifndef TEK_PREPROCESSOR_INC_H
