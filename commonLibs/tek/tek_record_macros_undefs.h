// Copyright 2008 Alexandre Courpron                                        
//                                                                          
// This file is part of the tek library.
// The tek library is licensed under the Code Project Open License v1.0.

#undef TEK_PP_RECORD_DATA_LIST
#undef TEK_PP_RECORD_DATA_LIST_END
#undef TEK_PP_RECORD_DATA_CTOR_PARAM
#undef TEK_PP_RECORD_DATA_CTOR
#undef TEK_PP_RECORD_PARAMS
#undef TEK_PP_RECORD_M_DATA_ARGS
#undef TEK_PP_RECORD_CTOR
#undef TEK_PP_RECORD_MERGE_APPLY
#undef TEK_PP_RECORD_APPLY
#undef TEK_PP_RECORD_MERGE_CASE
#undef TEK_PP_RECORD_CASE
#undef TEK_PP_RECORD_MERGE_APPLY_STRUCT
#undef TEK_PP_RECORD_APPLY_STRUCT

#undef TEK_UNFOLD_RECORD_TEMPLATE_PARAMS_NULLTYPE
#undef TEK_UNFOLD_RECORD_TEMPLATE_PARAMS
#undef TEK_UNFOLD_RECORD_DATA_TYPELIST
#undef TEK_UNFOLD_RECORD_CTOR
#undef TEK_UNFOLD_RECORD_DATA_CTOR
#undef TEK_UNFOLD_RECORD_TEMPLATE_PARAMS_NAME
#undef TEK_UNFOLD_RECORD_APPLY_STRUCT
#undef TEK_UNFOLD_RECORD_MERGE_APPLY_STRUCT