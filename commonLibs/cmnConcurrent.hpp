﻿#ifndef __HDR_CMNCONCURRENT__
#define __HDR_CMNCONCURRENT__

#include "cmnBase.hpp"

#include <qatomic.h>
#include <QMutex>
#include <QAtomicPointer>

namespace nsCmn
{
    namespace nsConcurrent
    {
        namespace nsDetail
        {

        }   // nsDetail


        // Double-Checked Locking 에 의한 싱글턴 구현
        template< typename T >
        class TSingleton
        {
        public:
            static T* GetInstance()
            {
                T* tmp = _pSelf.loadAcquire();
                if( tmp == NULLPTR )
                {
                    QMutexLocker guard( &_initSingletonMutex );

                    tmp = _pSelf.loadAcquire();
                    if( tmp == NULLPTR )
                    {
                        tmp = new T;
                        _pSelf.storeRelease( tmp );
                    }
                }

                return tmp;
            }

        protected:
            TSingleton() {};
            ~TSingleton() {};

        private:
            static QMutex                           _initSingletonMutex;
            static QAtomicPointer<T>                _pSelf;
        };

        template< typename T >
        QAtomicPointer<T> TSingleton<T>::_pSelf;

        template< typename T >
        QMutex TSingleton<T>::_initSingletonMutex;

    } // nsConcurrent
} // nsCmn


#endif //
