﻿#ifndef __HDR_CMN_DB_MGR__
#define __HDR_CMN_DB_MGR__

#include "cmnConcurrent.hpp"
#include "cmnDB_Defs.hpp"

#include <QtCore>
#include <QtSql>

// 전방선언
namespace pugi
{
    class xml_document;
    class xpath_node_set;
}

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {
            const QString PARAM_SCHEMA_TABLENAME    = "TBL_SCHEMAS";
            const QString PARAM_SCHEMA_COL_NAME     = "Name";
            const QString PARAM_SCHEMA_COL_REV      = "SchemaRev";
            const QString PARAM_SCHEMA_COL_ISINIT   = "IsInitDB";

            const QString SCHEMA_DBTYPE_ORACLE      = "ORACLE";
            const QString SCHEMA_DBTYPE_SQLITE      = "SQLITE";
            const QString SCHEMA_DBTYPE_ODBC        = "ODBC";
            const QString SCHEMA_DBTYPE_MSSQL       = "MSSQL";

            const QString PARAM_CURRENT_SCHEMA_REV  = "swdbSchemaRev";

            const QString SQL_SCHEMA_TABLE_DDL_SQLITE   = "CREATE TABLE IF NOT EXISTS %1 ( %2 TEXT NOT NULL PRIMARY KEY, %3 INTEGER DEFAULT 0, %4 INTEGER DEFAULT 0 )";
            const QString SQL_SCHEMA_TABLE_DDL_MSSQL    = "IF NOT EXISTS ( SELECT * FROM sysobjects WHERE id = object_id( N'[%1]' ) AND OBJECTPROPERTY(id, N'IsUserTable') = 1 ) "
                                                          "BEGIN "
                                                          " CREATE TABLE %1 ( %2 NVARCHAR(64) NOT NULL, %3 INT DEFAULT 0, %4 INT DEFAULT 0, PRIMARY KEY ( %2 ) );"
                                                          "END;";
            const QString SQL_SCHEMA_TABLE_DDL_ORACLE   = "CREATE TABLE %1 ( %2 NVARCHAR2(64) NOT NULL, %3 NUMBER(10,0) DEFAULT 0, %4 NUMBER(10,0) DEFAULT 0, PRIMARY KEY ( %2 ) )";

        }   // nsDetail

        class CCmnDBMgr
        {
        public:
            CCmnDBMgr();
            virtual ~CCmnDBMgr();

            bool                                    InitDBMS( const TyDatabaseOptions& tyDatabaseOptions );

            TySpCmnDB                               GetDefaultConnection( const QString& dbIdentity );
            TySpCmnDB                               GetConnection( const QString& dbIdentity = "" );

            void                                    SetDefaultDBIdentity( const QString& sDBIdentity );
            QString                                 GetDefaultDBIdentity();

            TyDatabaseOptions                       GetDBOptions( const QString& sDBIdentity );

        protected:

            QReadWriteLock                          m_lckDBOptions;
            TyMapDBIdentityToSchema                 m_mapDBIdentityToSchema;
            TyMapDBIdentityToOptions                m_mapDBIdentityToOptions;

            QReadWriteLock                          m_lckDBConnection;
            TyMapConnNameToConnection               m_mapConnNameToConnection;

        private:
            // DB 유형에 따라 최초 한번 시도해야할 DB 준비 작업
            virtual bool                            globalPrepareInit( const QString& sDBType );
            bool                                    doInit( const QString& sDBIdentity );
            QString                                 makeConnectionName( const QString& sDBIdentity = "" );

            //////////////////////////////////////////////////////////////////////////
            /// SCHEMA

            bool                                    loadDBMSSchema( const QString& sDBIdentity );
            bool                                    loadDBMSSchema( const QString& sDBIdentity, const pugi::xml_document& xmlDocument );
            void                                    loadSQLFromXML( const pugi::xpath_node_set& xpathNodeSet, QVector<QString>& vecSQL, QHash< QString, QString >& mapIDtoSQL );
            void                                    loadMigrationSQLFromXML( const pugi::xpath_node_set& xpathNodeSet, QHash< unsigned int, QVector< QString > >& mapTargetToSQLs );
            bool                                    checkSchemaTable( TySpCmnDB db, const QString& dbSchemaName );
            virtual bool                            createTables( TySpCmnDB db );
            virtual bool                            createSchemaFromXML( TySpCmnDB db, const QVector<QString>& vecSQL, bool isCommitPerSQL = false );
            virtual bool                            createUserANDSwitchUserSchema( TySpCmnDB db, const QString& username, const QString& password );
            virtual bool                            processMigrationSchemaFromXML( TySpCmnDB db, const nsDetail::TyMapRevToSQLs& mapTargetRevToSQLs, int currentDBRev );
            bool                                    writeSchemaRevTodB( TySpCmnDB db, const nsDetail::CMN_DB_SCHEMA& dbSchema, bool isSuccessToInit );

            QString                                 m_sSelectDBIdentity;

            bool                                    m_isInitOpts;
        };

    } // nsCmnDB
} // nsCmn

typedef nsCmn::nsConcurrent::TSingleton< nsCmn::nsCmnDB::CCmnDBMgr >            TyStCmnDBMgr;


#endif // __HDR_CMN_DB_MGR__