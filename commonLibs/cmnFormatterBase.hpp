﻿#ifndef __HDR_CMN_FORMATTER_BASE__
#define __HDR_CMN_FORMATTER_BASE__

#include "cmnTypeTraits.hpp"

#include <boost/mpl/eval_if.hpp>
#include <boost/mpl/identity.hpp>

namespace nsCmn
{
    namespace nsCmnFormatter
    {
        namespace nsDetail
        {
            const int                               DEFAULT_BUFFER_SIZE = 256;
        } // nsDetail

        const char* const                           DEFAULT_DATE_FORMAT_A = "%Y-%m-%d";
        const char* const                           DEFAULT_TIME_FORMAT_A = "%H:%M:%S";
        const char* const                           DEFAULT_DATE_TIME_FORMAT_A = "%Y-%m-%d %H:%M:%S";

        const wchar_t* const                        DEFAULT_DATE_FORMAT_W = L"%Y-%m-%d";
        const wchar_t* const                        DEFAULT_TIME_FORMAT_W = L"%H:%M:%S";
        const wchar_t* const                        DEFAULT_DATE_TIME_FORMAT_W = L"%Y-%m-%d %H:%M:%S";

    } // nsCmnFormatter
} // nsCmn

namespace nsCmn
{
    namespace nsCmnFormatter
    {
        namespace nsDetail
        {
            // Remove const and volatile qualifiers from the type, to
            // minimize the number of specializations of type_tag
            // required (e.g. char*, const char*, volatilie char*, etc).
            // We can't just use boost::remove_cv<> here, because that
            // doesn't do what we want for pointers.  It removes the
            // const and volatile qualifiers from the pointer, but not
            // from the pointed-to type.
            template<typename T>
            struct basic_type
            {
            private:
                typedef typename nsCmn::nsTraits::remove_reference<T>::type Ty;
                
            public:
                typedef typename boost::mpl::eval_if <
                    nsCmn::nsTraits::is_pointer<Ty>,
                    std::add_pointer<
                        typename std::remove_cv<
                        typename std::remove_pointer<Ty>::type
                        >::type
                    >,
                    boost::mpl::identity < Ty >
                > ::type type;
            };

        } // nsDetail

        enum Alignment 
        { 
            ALIGN_DEFAULT, 
            ALIGN_LEFT, 
            ALIGN_RIGHT, 
            ALIGN_CENTER, 
            ALIGN_NUMERIC 
        };

        typedef struct tagFormatSpec
        {
            long                                    argIndex;

            // char 로 캐스팅 가능하도록 wchar_t 로 선언
            wchar_t                                 fillChar;
            Alignment                               align;

        } FORMAT_SPEC;

    } // nsCmnFormatter
} // nsCmn

#endif // __HDR_CMN_FORMATTER_BASE__