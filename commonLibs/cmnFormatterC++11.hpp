﻿#ifndef __HDR_CMN_FORMATTER_CPP11__
#define __HDR_CMN_FORMATTER_CPP11__

#include "cmnBase.hpp"
#include "cmnContainers.hpp"
#include "cmnFormatterWriter.hpp"

#if defined(_MSC_VER)
#   pragma execution_character_set( "utf-8" )
#endif

#if CMN_USE_VARIADIC_TEMPLATES > 0

namespace nsCmn
{
    namespace nsCmnFormatter
    {
        namespace nsDetail
        {

        } // nsDetail

        template< typename ... Args >
        inline std::string tsFormat( nsCmn::StringRef fmt, Args ... args )
        {
            MemoryWriter  w;
            w.format( fmt, args... );
            return w.str();
        };

        template< typename ... Args >
        inline std::wstring tsFormat( nsCmn::WStringRef fmt, Args ... args )
        {
            WMemoryWriter  w;
            w.format( fmt, args... );
            return w.str();
        };

        template< typename ... Args >
        inline void tsFormat( MemoryWriter& writer, nsCmn::StringRef fmt, Args ... args )
        {
            writer.clear();
            writer.format( fmt, args... );
        }

        template< typename ... Args >
        inline void tsFormat( WMemoryWriter& writer, nsCmn::WStringRef fmt, Args ... args )
        {
            writer.clear();
            writer.format( fmt, args... );
        }

        template< typename ... Args >
        inline void tsFormat( ArrayWriter& writer, nsCmn::StringRef fmt, Args ... args )
        {
            writer.clear();
            writer.format( fmt, args... );
        }

        template< typename ... Args >
        inline void tsFormat( WArrayWriter& writer, nsCmn::WStringRef fmt, Args ... args )
        {
            writer.clear();
            writer.format( fmt, args... );
        }

        template< typename CharT, typename ... Args >
        size_t tsFormat( CharT* dst, size_t size, nsCmn::BasicStringRef< CharT > fmt, Args ... args )
        {
            BasicArrayWriter< CharT > w( dst, size );
            w.format( fmt, args... );
            return w.size();
        }

    } // nsCmnFormatter

} // nsCmn

#endif

#endif
