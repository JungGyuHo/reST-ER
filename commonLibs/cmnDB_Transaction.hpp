﻿#ifndef __HDR_CMN_DB_TRANSACTION__
#define __HDR_CMN_DB_TRANSACTION__

#include "cmnDB_Defs.hpp"

#include <QtCore>

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {
            // 스레드당 축적되는 트랜잭션 스택( QThreadStorage 를 사용하여 스레드당 생성되도록 한다 )
            typedef QStack< QString >               TyStkTransaction;

        }   // nsDetail

        /**
        * A basic (default, deferred) transaction.
        */
        class CCmnDBBasicTransaction
        {
            Q_DISABLE_COPY( CCmnDBBasicTransaction );

        public:

            /**
            * Constructor that provides a connection upon which to act.
            *
            * @param connection a connection
            */
            explicit CCmnDBBasicTransaction( TySpCmnDB connection );
            virtual ~CCmnDBBasicTransaction();

        public:

            /**
            * Begin the transaction
            */
            virtual bool begin();

            /**
            * Commit the transaction
            */
            virtual bool commit();

            /**
            * Rollback the transaction
            */
            virtual bool rollback();

            QSqlError                               getLastError();

        protected:
            /** reset any in-progress statements */
            void reset_active_queries();

            /** the connection on which to act */
            TySpCmnDB                               _connection;
            QSqlError                               _lasterror;
        };

        /**
        * An immediate transaction.
        */
        class CCmnDBImmediateTransaction : public CCmnDBBasicTransaction
        {
            //______________________________________________________________________________
            //                                                                 instantiation
        public:

            /**
            * Constructor that provides a connection upon which to act.
            *
            * @param connection a connection
            */
            explicit CCmnDBImmediateTransaction( TySpCmnDB connection );
            virtual ~CCmnDBImmediateTransaction();

            //______________________________________________________________________________
            //                                                              public interface
        public:

            /**
            * Begin the transaction.
            */
            virtual bool begin();

        };

        /**
        * A recursive transaction, allowing transactions to be nested.
        */
        class CCmnDBRecursiveTransaction : public CCmnDBImmediateTransaction
        {
            //______________________________________________________________________________
            //                                                                 instantiation
        public:

            /**
            * Constructor that provides a connection upon which to act.
            *
            * @param connection a connection
            */
            explicit CCmnDBRecursiveTransaction( TySpCmnDB connection );
            virtual ~CCmnDBRecursiveTransaction();

            //______________________________________________________________________________
            //                                                              public interface
        public:

            /**
            * Begin the transaction.
            */
            virtual bool begin();

            /**
            * Commit the transaction.
            */
            virtual bool commit();

            /**
            * Rollback the transaction.
            */
            virtual bool rollback();

        protected:
            /* this transaction's savepoint name */
            QString                                 _sp_name;
            bool                                    _isInitialEmptyStack;
            bool                                    _isInitialTransaction;
        };

        ////////////////////////////////////////////////////////////////////////////////

        /**
        * A scope guard (sentinel) for use with one of the transaction classes to
        * provide RAII-style transactions.  It defaults to using deferred transactions.
        */
        template< class T = CCmnDBRecursiveTransaction >
        class CCmnDBTransactionGuard
        {
            Q_DISABLE_COPY( CCmnDBTransactionGuard );
            //______________________________________________________________________________
            //                                                                 instantiation
        public:

            /**
            * Constructor that provides a connection upon which to act.
            *
            * @param connection a connection
            */
            explicit CCmnDBTransactionGuard( TySpCmnDB &connection )
                :
                _transaction( connection ), _issuccess( false ),
                _released( true )
                , _isSuccessToSQL( false )
            {
                _issuccess = begin();
                _released = false;
            }

            ~CCmnDBTransactionGuard()
            {
                apply();
            }

            CCmnDBTransactionGuard& operator = ( bool isSucces )
            {
                setSuccessToSQL( isSucces );
                return *this;
            }

            bool operator == ( bool rhs )
            {
                return _isSuccessToSQL == rhs;
            }

            void                                    setSuccessToSQL( bool isSuccessToSQL )
            {
                _isSuccessToSQL = isSuccessToSQL;
            }

            bool                                    isSuccessToSQL()
            {
                return _isSuccessToSQL;
            }

            // 쿼리 수행결과에 따른 최종결과에 맞게 commit 또는 rollback 을 수행한 후 결과를 반환한다. 
            // @return true, 쿼리가 성공하고 commit 이 성공함
            // @return false, 쿼리가 성공했지만, commit 이 실패함
            // @return false, 쿼리가 실패하고 rollback 이 성공함
            // @return false, 쿼리가 실패하고 rollback 도 실패함 
            bool apply()
            {
                std::pair< bool, bool > prResult = applyDivisionResult();
                return prResult.first == true && prResult.second == true;
            }

            // @return first = SQL 수행 결과, second = 트랜잭션 반영 결과
            std::pair< bool, bool > applyDivisionResult()
            {
                if( !_released )
                {
                    if( _isSuccessToSQL == false )
                        _issuccess = rollback();
                    else
                        _issuccess = commit();
                }

                return std::make_pair( _isSuccessToSQL, _issuccess );
            }

            //______________________________________________________________________________
            //                                                              public interface
        public:

            /**
            * Begin the transaction. Note that this is done automatically in the
            * constructor.
            */
            bool begin()
            {
                _issuccess = false;
                if( _released )
                {
                    _issuccess = _transaction.begin();
                    _released = false;
                }
                return _issuccess;
            }

            /**
            * Commit the transaction.
            */
            bool commit()
            {
                _issuccess = false;
                if( !_released )
                {
                    _issuccess = _transaction.commit();
                    _released = true;
                }
                return _issuccess;
            }

            /**
            * Rollback the transaction. Note that this is done automatically in the
            * destructor if the transaction hasn't otherwise been completed.
            */
            bool rollback()
            {
                _issuccess = false;
                if( !_released )
                {
                    _issuccess = _transaction.rollback();
                    _released = true;
                }
                return _issuccess;
            }

#if defined( USE_QT_SUPPORT ) && defined( QT_QTSQL_MODULE_H )
            QSqlError                               getLastError()
            {
                return _transaction.getLastError();
            }
#endif

            //______________________________________________________________________________
            //                                                                implementation
        protected:

            /** the transaction */
            T _transaction;

            /** have we released the transaction yet? */
            bool _released;
            bool _issuccess;

            // SQL 쿼리 성공결과에 따라 자동으로 commit 또는 rollback 호출
            bool _isSuccessToSQL;
        };

    } // nsCmnDB
} // nsCmn

#endif // __HDR_CMN_DB_TRANSACTION__