﻿#ifndef __HDR_CMN_DB_DEFS__
#define __HDR_CMN_DB_DEFS__

#include <QString>
#include <QHash>
#include <QMap>
#include <QVector>
#include <QSharedPointer>

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {
            // key = 마이그레이션 대상 rev, value = order 별로 정렬된 SQL 목록
            typedef QHash< unsigned int, QVector< QString > > TyMapRevToSQLs;

            typedef struct
            {
                unsigned int                        rev;        // schema dtd rev
                unsigned int                        schemaRev;  // schema rev

                QString                             sDBType;
                QString                             sDBName;    // MS SQL CATALOG 

                // schema rev 1
                QVector< QString >                  vecCreateDBSQLs;
                QVector< QString >                  vecCreateUserSQLs;
                QVector< QString >                  vecCreateTables;
                QVector< QString >                  vecCreateFunctions;
                QVector< QString >                  vecCreateViews;
                QVector< QString >                  vecCreateIndexies;
                QVector< QString >                  vecPostScripts;
                QVector< QString >                  vecDefaultDatas;
                QVector< QString >                  vecMigrationSQLs;                                  // 스키마 정의 규격 1

                // first = userName, second = password ( createUser 태그의 username, password 저장 )
                QPair< QString, QString >           prCreateUserInfo;

                // schema rev 2
                TyMapRevToSQLs                      mapTargetRevToMigrationSQLs;  // 스키마 정의 규격 2

                // schema rev 3
                QString                             sSchemaName;

                // schema rev 4
                // order 를 지정하지 않으면 순차적으로 처리 
                // id 속성 도입, ref 를 통해 id 를 지정한 쿼리 참조

                QHash< QString, QString >           mapIDToSQL;

            } CMN_DB_SCHEMA;

        }   // nsDetail

        struct TyDatabaseOptions 
        {
            QString                                 sDBIdentity;    // unique identifier
            QString                                 sDBType;        // QtSQL 의 드라이버 이름( QSQLITE, QODBC ... )
            QString                                 sDBName;        // SQLITE 의 경우는 DB 파일 이름 및 경로

            QString                                 sHostName;      // Not Used SQLITE
            unsigned short                          usHostPort;     // Not Used SQLITE

            QString                                 sAccountName;
            QString                                 sAccountPass;

            QString                                 sSchemaFileName;
            QHash< QString, QString >               mapExtraKVPairs;
        };

        // memory db 사용여부 Y/N, 메모리 DB 를 사용하면 dbName 에서 파일이름 부분을 memory db 의 이름으로 사용한다. 
        const QString DBMS_OPT_SQLITE_INMEMORY      = "sqliteMemoryDB";
        const QString DBMS_OPT_SQLITE_CRYPTO_KEY    = "sqliteCryptoKey";
        // 해당 값을 설정하면 리소스 또는 파일에서 스키마 파일을 가져오지 않고, DB OPTIONS 으로 전달된 스키마 데이터를 사용함
        const QString DBMS_OPT_SCHEMA_DATA          = "dbSchemaXMLData";

        class CCmnDB;
        typedef QSharedPointer< CCmnDB >            TySpCmnDB;

        typedef QMap< QString, TyDatabaseOptions >  TyMapDBIdentityToOptions;
        typedef QMap< QString, nsDetail::CMN_DB_SCHEMA >    TyMapDBIdentityToSchema;
        typedef QHash< QString, TySpCmnDB >         TyMapConnNameToConnection;

        typedef QPair< QString, QVariant >          TyPrColumnNameToValue;
    } // nsCmnDB
} // nsCmn


#endif // __HDR_CMN_DB_DEFS__