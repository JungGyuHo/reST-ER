﻿#ifndef __HDR_CMN_COMPILER_DETECTION__
#define __HDR_CMN_COMPILER_DETECTION__

#if defined(_MSC_VER)
#   pragma execution_character_set( "utf-8" )
#endif

#ifdef _SECURE_SCL
# define CMN_SECURE_SCL _SECURE_SCL
#else
# define CMN_SECURE_SCL 0
#endif

#ifdef _MSC_VER
# define CMN_MSC_VER _MSC_VER
#else
# define CMN_MSC_VER 0
#endif

#define _MSVC2008_VER 1500
#define _MSVC2010_VER 1600
#define _MSVC2012_VER 1700
#define _MSVC2013_VER 1800
#define _MSVC2015_VER 1900
#define _MSVC2017_VER 1910

#ifdef __GNUC__
    #define CMN_GCC_VERSION (__GNUC__ * 100 + __GNUC_MINOR__)
    #define CMN_GCC_EXTENSION __extension__
    #if CMN_GCC_VERSION >= 406
        #pragma GCC diagnostic push
        // Disable the warning about "long long" which is sometimes reported even
        // when using __extension__.
        #pragma GCC diagnostic ignored "-Wlong-long"
        // Disable the warning about declaration shadowing because it affects too
        // many valid cases.
        #pragma GCC diagnostic ignored "-Wshadow"
        // Disable the warning about implicit conversions that may change the sign of
        // an integer; silencing it otherwise would require many explicit casts.
        #pragma GCC diagnostic ignored "-Wsign-conversion"
    #endif
    #if __cplusplus >= 201103L || defined __GXX_EXPERIMENTAL_CXX0X__
        #define CMN_HAS_GXX_CXX11 1
    #endif

    #define W(x)          W_(x)
    #define W_(x)         L ## x

    #define LOCATION_(t)  t(__FILE__)
    #define FUNCTION_(t)  t(__FUNCTION__)

    #ifndef __FILEW__ 
        #define __FILEW__ LOCATION_(W)
    #endif
    #ifndef __FUNCTIONW__ 
        #define __FUNCTIONW__ L""
    #endif
#else
# define CMN_GCC_EXTENSION
#endif

#if defined(__GNUC_LIBSTD__) && defined (__GNUC_LIBSTD_MINOR__)
# define CMN_GNUC_LIBSTD_VERSION (__GNUC_LIBSTD__ * 100 + __GNUC_LIBSTD_MINOR__)
#endif

#ifdef __has_feature
# define CMN_HAS_FEATURE(x) __has_feature(x)
#else
# define CMN_HAS_FEATURE(x) 0
#endif

#ifdef __has_builtin
# define CMN_HAS_BUILTIN(x) __has_builtin(x)
#else
# define CMN_HAS_BUILTIN(x) 0
#endif

#ifdef __has_cpp_attribute
# define CMN_HAS_CPP_ATTRIBUTE(x) __has_cpp_attribute(x)
#else
# define CMN_HAS_CPP_ATTRIBUTE(x) 0
#endif

#ifndef CMN_USE_VARIADIC_TEMPLATES
// Variadic templates are available in GCC since version 4.4
// (http://gcc.gnu.org/projects/cxx0x.html) and in Visual C++
// since version 2013.
# define CMN_USE_VARIADIC_TEMPLATES \
   (CMN_HAS_FEATURE(cxx_variadic_templates) || \
       (CMN_GCC_VERSION >= 404 && CMN_HAS_GXX_CXX11) || CMN_MSC_VER >= _MSVC2013_VER)
#endif

#ifndef CMN_USE_RVALUE_REFERENCES
// Don't use rvalue references when compiling with clang and an old libstdc++
// as the latter doesn't provide std::move.
# if defined(CMN_GNUC_LIBSTD_VERSION) && CMN_GNUC_LIBSTD_VERSION <= 402
#  define CMN_USE_RVALUE_REFERENCES 0
# else
#  define CMN_USE_RVALUE_REFERENCES \
    (CMN_HAS_FEATURE(cxx_rvalue_references) || \
        (CMN_GCC_VERSION >= 403 && CMN_HAS_GXX_CXX11) || CMN_MSC_VER >= _MSVC2010_VER)
# endif
#endif

// Check if exceptions are disabled.
#if defined(__GNUC__) && !defined(__EXCEPTIONS)
# define CMN_EXCEPTIONS 0
#endif
#if CMN_MSC_VER && !_HAS_EXCEPTIONS
# define CMN_EXCEPTIONS 0
#endif
#ifndef CMN_EXCEPTIONS
# define CMN_EXCEPTIONS 1
#endif

#ifndef CMN_THROW
# if CMN_EXCEPTIONS
#  define CMN_THROW(x) throw x
# else
#  define CMN_THROW(x) assert(false)
# endif
#endif

// Define FMT_USE_NOEXCEPT to make fmt use noexcept (C++11 feature).
#ifndef CMN_USE_NOEXCEPT
# define CMN_USE_NOEXCEPT 0
#endif

#ifndef CMN_NOEXCEPT
# if CMN_EXCEPTIONS
#  if CMN_USE_NOEXCEPT || CMN_HAS_FEATURE(cxx_noexcept) || \
    (CMN_GCC_VERSION >= 408 && CMN_HAS_GXX_CXX11) || \
    CMN_MSC_VER >= _MSVC2015_VER
#   define CMN_NOEXCEPT noexcept
#  else
#   define CMN_NOEXCEPT throw()
#  endif
# else
#  define CMN_NOEXCEPT
# endif
#endif

#if CMN_EXCEPTIONS
# define CMN_TRY try
# define CMN_CATCH(x) catch (x)
#else
# define CMN_TRY if (true)
# define CMN_CATCH(x) if (false)
#endif

// 
// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#ifndef CMN_USE_DELETED_FUNCTIONS
# define CMN_USE_DELETED_FUNCTIONS 0
#endif

#if CMN_USE_DELETED_FUNCTIONS || CMN_HAS_FEATURE(cxx_deleted_functions) || \
  (CMN_GCC_VERSION >= 404 && CMN_HAS_GXX_CXX11) || CMN_MSC_VER >= _MSVC2013_VER
# define CMN_DELETED_OR_UNDEFINED  = delete
# define CMN_DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&) = delete; \
    TypeName& operator=(const TypeName&) = delete
#else
# define CMN_DELETED_OR_UNDEFINED
# define CMN_DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&); \
    TypeName& operator=(const TypeName&)
#endif

#ifndef CMN_USE_USER_DEFINED_LITERALS
// All compilers which support UDLs also support variadic templates. This
// makes the fmt::literals implementation easier. However, an explicit check
// for variadic templates is added here just in case.
// For Intel's compiler both it and the system gcc/msc must support UDLs.
# define CMN_USE_USER_DEFINED_LITERALS \
   CMN_USE_VARIADIC_TEMPLATES && CMN_USE_RVALUE_REFERENCES && \
   (CMN_HAS_FEATURE(cxx_user_literals) || \
     (CMN_GCC_VERSION >= 407 && CMN_HAS_GXX_CXX11) || CMN_MSC_VER >= _MSVC2015_VER)
#endif

#ifndef CMN_USE_UNIQUE_PTR
#if defined(__GNUC__) || (CMN_MSC_VER >= _MSVC2010_VER)
#define CMN_USE_UNIQUE_PTR 1 
#else 
#define CMN_USE_UNIQUE_PTR 0
#endif
#endif

#endif // __HDR_CMN_COMPILER_DETECTION__