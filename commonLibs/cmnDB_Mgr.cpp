﻿#include "stdafx.h"
#include "cmnDB_Mgr.hpp"
#include "cmnDB.hpp"
#include "cmnDB_Transaction.hpp"

#define PUGIXML_HEADER_ONLY
#include "pugixml.hpp"

//////////////////////////////////////////////////////////////////////////

namespace nsCmn
{
    namespace nsCmnDB
    {
        using namespace nsDetail;

        CCmnDBMgr::CCmnDBMgr()
            : m_lckDBOptions( QReadWriteLock::Recursive )
            , m_lckDBConnection( QReadWriteLock::Recursive )

            , m_isInitOpts(false)
        {

        }

        CCmnDBMgr::~CCmnDBMgr()
        {

        }

        bool CCmnDBMgr::InitDBMS( const TyDatabaseOptions& tyDatabaseOptions )
        {
            bool isSuccess = false;

            do 
            {
                Q_ASSERT( tyDatabaseOptions.sDBIdentity.isEmpty() == false );
                Q_ASSERT( tyDatabaseOptions.sDBType.isEmpty() == false );
                Q_ASSERT( tyDatabaseOptions.sDBName.isEmpty() == false );

                if( QSqlDatabase::isDriverAvailable( tyDatabaseOptions.sDBType ) == false )
                {
                    // TODO: 오류로그 출력
                    break;
                }

                {
                    QReadLocker lock( &m_lckDBOptions );
                    if( m_mapDBIdentityToOptions.contains( tyDatabaseOptions.sDBIdentity ) == true )
                    {
                        // TODO: 오류로그 출력
                        break;
                    }
                }

                {
                    QWriteLocker lock( &m_lckDBOptions );
                    m_mapDBIdentityToOptions[tyDatabaseOptions.sDBIdentity] = tyDatabaseOptions;
                }
                
                if( m_isInitOpts == false )
                {
                    m_isInitOpts = globalPrepareInit( tyDatabaseOptions.sDBType );
                    if( m_isInitOpts == false )
                        break;
                }

                isSuccess = doInit( tyDatabaseOptions.sDBIdentity );

                if( isSuccess == true )
                {
                    if( m_sSelectDBIdentity.isEmpty() == true )
                        SetDefaultDBIdentity( tyDatabaseOptions.sDBIdentity );
                }
                else
                {
                    QWriteLocker lock( &m_lckDBOptions );
                    m_mapDBIdentityToOptions.remove( tyDatabaseOptions.sDBIdentity );
                    m_mapDBIdentityToSchema.remove( tyDatabaseOptions.sDBIdentity );
                }

            } while (false);

            return isSuccess;
        }

        nsCmn::nsCmnDB::TySpCmnDB CCmnDBMgr::GetConnection( const QString& dbIdentity /*= "" */ )
        {
            nsCmn::nsCmnDB::TySpCmnDB spConnection;
            QString sConnName = makeConnectionName( dbIdentity );
            QSqlDatabase db = QSqlDatabase::database( sConnName );

            do 
            {
                if( db.isOpen() == true && db.isValid() == true )
                {
                    QReadLocker lck( &m_lckDBConnection );
                    if( m_mapConnNameToConnection.contains( sConnName ) == true )
                    {
                        spConnection = m_mapConnNameToConnection[sConnName];
                        break;
                    }
                    else
                    {
                        // TODO: 오류 로그 출력, DB 가 유효한다면 맵에 들어가 있어야함
                        Q_ASSERT( false );
                    }
                }

                QReadLocker lck( &m_lckDBOptions );
                TyDatabaseOptions dbOptions = m_mapDBIdentityToOptions[dbIdentity];
                db = QSqlDatabase::addDatabase( dbOptions.sDBType, sConnName );

                //////////////////////////////////////////////////////////////////////////
                /// Database Setting

                db.setDatabaseName( dbOptions.sDBName );

                if( dbOptions.sDBName.compare( "QSQLITE", Qt::CaseInsensitive ) == 0 )
                {

                }
                else if( dbOptions.sDBName.compare( "QODBC", Qt::CaseInsensitive ) == 0 )
                {

                }

                if( db.open() == false )
                {
                    // TODO: 오류 로그 출력
                    break;
                }

                /// DBMS 후처리

                if( dbOptions.sDBType.compare( "QSQLITE", Qt::CaseInsensitive ) == 0 )
                {
                    QHash< QString, QString > mapExtraKVPairs = dbOptions.mapExtraKVPairs;
                    if( mapExtraKVPairs.contains( DBMS_OPT_SQLITE_CRYPTO_KEY ) == true )
                    {
                        // PRAGMA KEY='{}'
                    }

                    // PRAGMA journal_mode=WAL;
                    // PRAGMA busy_timeout=30000;
                }

                //////////////////////////////////////////////////////////////////////////
                /// 

                {
                    QWriteLocker lock( &m_lckDBConnection );
                    spConnection = TySpCmnDB( new CCmnDB( db ) );
                    spConnection->SetDBConnectionText( sConnName );
                    spConnection->SetDBIdentity( dbOptions.sDBIdentity );
                    spConnection->SetDBType( dbOptions.sDBType );
                    m_mapConnNameToConnection[sConnName] = spConnection;
                }

            } while (false);

            return spConnection;
        }

        void CCmnDBMgr::SetDefaultDBIdentity( const QString& sDBIdentity )
        {
            m_sSelectDBIdentity = sDBIdentity;
        }

        QString CCmnDBMgr::GetDefaultDBIdentity()
        {
            return m_sSelectDBIdentity;
        }

        nsCmn::nsCmnDB::TyDatabaseOptions CCmnDBMgr::GetDBOptions( const QString& sDBIdentity )
        {
            QReadLocker lock( &m_lckDBOptions );
            if( m_mapDBIdentityToOptions.contains( sDBIdentity ) == false )
                return nsCmn::nsCmnDB::TyDatabaseOptions();

            return m_mapDBIdentityToOptions[sDBIdentity];
        }

        bool CCmnDBMgr::globalPrepareInit( const QString& sDBType )
        {
            bool isSuccess = false;

            do 
            {
                if( sDBType.compare( "QSQLITE", Qt::CaseInsensitive ) == 0 )
                {

                }

                isSuccess = true;

            } while (false);

            return isSuccess;
        }

        //////////////////////////////////////////////////////////////////////////

        bool CCmnDBMgr::doInit( const QString& sDBIdentity )
        {
            bool isSuccess = false;
            bool isInitDBNeo = false;

            do 
            {
                TySpCmnDB spConn = GetConnection( sDBIdentity );
                if( spConn == NULLPTR || spConn->IsOpen() == false )
                    break;

                if( loadDBMSSchema( sDBIdentity ) == false )
                    break;

                QWriteLocker lck( &m_lckDBOptions );
                if( m_mapDBIdentityToOptions.contains( sDBIdentity ) == false )
                    break;

                TyDatabaseOptions dbOptions = m_mapDBIdentityToOptions[sDBIdentity];
                nsDetail::CMN_DB_SCHEMA dbSchemas = m_mapDBIdentityToSchema[sDBIdentity];

                unsigned int currentDBRev = 0;
                if( dbSchemas.rev >= 3 )
                {
                    if( checkSchemaTable( spConn, dbSchemas.sSchemaName ) == true )
                    {
                        // DB 테이블에서 현재 리비전을 읽어옴
                        QString query = QString( "SELECT * FROM %1 WHERE %2 = '%3'" ).arg( nsDetail::PARAM_SCHEMA_TABLENAME ).arg( nsDetail::PARAM_SCHEMA_COL_NAME ).arg( dbSchemas.sSchemaName );
                        QSqlQuery dbStmt = spConn->Exec( query );
                        if( dbStmt.next() == false )
                            currentDBRev = 0;
                        else
                        {
                            QSqlRecord rec = dbStmt.record();
                            currentDBRev = dbStmt.value( rec.indexOf( nsDetail::PARAM_SCHEMA_COL_REV ) ).toUInt();
                            isInitDBNeo = dbStmt.value( rec.indexOf( nsDetail::PARAM_SCHEMA_COL_ISINIT ) ).toBool();
                        }
                    }
                    else
                    {
                        currentDBRev = 0;
                    }
                }
//                 else
//                 {
//                     currentDBRev = nsCmn::stoi( dbOptions[PARAM_CURRENT_SWDB_SCHEMA_REV] );
//                 }
// 
//                 // 설정에 최초의 스키마 리비전을 0으로 설정한 경우 스키마를 생성한 후 리비전이 정상적으로 기록되도록 함
//                 if( currentDBRev == 0 && isInitDBNeo == false && isSuccess == true )
//                 {
//                     dbOptions[PARAM_CURRENT_SWDB_SCHEMA_REV] = tsFormat( L"{}", dbSchemas.schemaRev );
//                     currentDBRev = dbSchemas.schemaRev;
//                 }
// 
                lck.unlock();

                if( isInitDBNeo == false )
                {
                    // 기본 스키마 생성
                    IF_FALSE_BREAK( isSuccess, createTables( spConn ) );
                    spConn = GetConnection( spConn->GetDBIdentity() );
                }

                isSuccess = true;
                if( dbSchemas.schemaRev > currentDBRev && isInitDBNeo == true )
                {
                    // 현재 적용된 스키마가 있고 XML 의 스키마가 현재 적용된 스키마보다 높다면 마이그레이션 시작
                    if( dbSchemas.rev <= 1 )
                    {
                        IF_FALSE_BREAK( isSuccess, createSchemaFromXML( spConn, dbSchemas.vecMigrationSQLs ) );
                    }
                    else if( dbSchemas.rev > 1 )
                    {
                        IF_FALSE_BREAK( isSuccess, processMigrationSchemaFromXML( spConn, dbSchemas.mapTargetRevToMigrationSQLs, currentDBRev ) );
                    }
                }

                lck.relock();
//                 dbOptions[PARAM_CURRENT_SWDB_SCHEMA_REV] = tsFormat( L"{}", dbSchemas.schemaRev );
                lck.unlock();

                // DB 에 스키마 리비전 기록
                if( dbSchemas.rev >= 3 )
                {
                    writeSchemaRevTodB( spConn, dbSchemas, isSuccess );
                }

                isSuccess = true;
            } while (false);

            return isSuccess;
        }

        QString CCmnDBMgr::makeConnectionName( const QString& sDBIdentity /*= "" */ )
        {
            Q_ASSERT( sDBIdentity.isEmpty() == false || m_sSelectDBIdentity.isEmpty() == false );

            return QString("conn-%1-%2").arg( sDBIdentity ).arg( GetCurrentThreadId() );
        }

        bool CCmnDBMgr::loadDBMSSchema( const QString& sDBIdentity )
        {
            bool isSuccess = false;
            QByteArray readData;

            do
            {
                TyDatabaseOptions dbOptions = GetDBOptions( sDBIdentity );

                if( dbOptions.mapExtraKVPairs.contains( DBMS_OPT_SCHEMA_DATA ) == true )
                {
                    readData = dbOptions.mapExtraKVPairs[DBMS_OPT_SCHEMA_DATA].toLatin1();
                }
                else
                {
                    QFile schemaFile;
                    QFileInfo schemaFileInfo( qApp->applicationDirPath(), dbOptions.sSchemaFileName );
                    if( QFile::exists( schemaFileInfo.absoluteFilePath() ) == true )
                        schemaFile.setFileName( schemaFileInfo.absoluteFilePath() );
                    else
                        schemaFile.setFileName( dbOptions.sSchemaFileName );

                    if( schemaFile.open( QIODevice::Text | QIODevice::ReadOnly ) == false )
                        break;

                    readData = schemaFile.readAll();
                }

                pugi::xml_document xmlDocument;
                pugi::xml_parse_result ret = xmlDocument.load_buffer( readData.constData(), readData.size() );
                if( ret.status != pugi::status_ok )
                {
                    // TODO: 오류 로그 출력
//                     qDebug() << "cmnDBSchema File Loading Error!!! " << ret.description();
                    break;
                }

                if( (isSuccess = loadDBMSSchema( sDBIdentity, xmlDocument )) == false )
                {
//                     LM_ERROR_TO( cmndbmanager_logger, L"DBMS 스키마 설정이 실패하였습니다" );
                    break;
                }

            } while( false );
            return isSuccess;
        }

        bool CCmnDBMgr::loadDBMSSchema( const QString& sDBIdentity, const pugi::xml_document& xmlDocument )
        {
            bool isSuccess = false;

            do 
            {
                /*!
                    # 스키마 정의를 구조체로 불러오기

                    # XML의 스키마 리비전과 현재 적용된 스키마 리비전 비교
                    # XML의 스키마 리비전이 더 높다면 마이그레이션 시작

                */
                QWriteLocker lock( &m_lckDBOptions );
                nsDetail::CMN_DB_SCHEMA& dbSchemas = m_mapDBIdentityToSchema[sDBIdentity];

                pugi::xml_node sqlSchema = xmlDocument.select_single_node( "//SQL" ).node();
                pugi::xml_node nodeSchemaMain = xmlDocument.select_single_node( "//schemaDefenitions" ).node();

                dbSchemas.rev = sqlSchema.attribute( "rev" ).as_uint();
                dbSchemas.schemaRev = nodeSchemaMain.attribute( "rev" ).as_uint();
                dbSchemas.sDBType = nodeSchemaMain.attribute( "dbType" ).as_string();
                if( dbSchemas.rev >= 3 )
                {
                    dbSchemas.sSchemaName = QString::fromStdString( nodeSchemaMain.attribute( "schemaName" ).as_string() );
                    if( dbSchemas.sSchemaName.isEmpty() == true )
                    {
                        // TODO: 오류 로그 출력
//                         LM_ERROR_TO( cmndbmanager_logger, L"DBMS 스키마 리비전 3에는 스키마 이름(schemaName) 항목이 필수입니다" );
                        break;
                    }
                }

                dbSchemas.sDBName = QString::fromStdString( xmlDocument.select_single_node( "//createDB" ).node().attribute( "name" ).as_string() );

                pugi::xml_node xmlUserNode = xmlDocument.select_single_node( "//createUser" ).node();
                if( xmlUserNode.empty() == false )
                {
                    dbSchemas.prCreateUserInfo.first = QString::fromStdString( xmlUserNode.attribute( "username" ).as_string() );
                    dbSchemas.prCreateUserInfo.second = QString::fromStdString( xmlUserNode.attribute( "password" ).as_string() );
                }

                loadSQLFromXML( xmlDocument.select_nodes( "//createDB/sql" ), dbSchemas.vecCreateDBSQLs, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//createUser/sql" ), dbSchemas.vecCreateUserSQLs, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//createTables/sql" ), dbSchemas.vecCreateTables, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//createFunctions/sql" ), dbSchemas.vecCreateFunctions, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//createViews/sql" ), dbSchemas.vecCreateViews, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//createIndexies/sql" ), dbSchemas.vecCreateIndexies, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//postScript/sql" ), dbSchemas.vecPostScripts, dbSchemas.mapIDToSQL );
                loadSQLFromXML( xmlDocument.select_nodes( "//createDefaultDatas/sql" ), dbSchemas.vecDefaultDatas, dbSchemas.mapIDToSQL );

                if( dbSchemas.rev <= 1 )
                    loadSQLFromXML( xmlDocument.select_nodes( "//migrations/sql" ), dbSchemas.vecMigrationSQLs, dbSchemas.mapIDToSQL );
                else if( dbSchemas.rev >= 2 )
                    loadMigrationSQLFromXML( xmlDocument.select_nodes( "//migrations/toRev" ), dbSchemas.mapTargetRevToMigrationSQLs );

                isSuccess = true;

            } while (false);

            return isSuccess;            
        }

        void CCmnDBMgr::loadSQLFromXML( const pugi::xpath_node_set& xpathNodeSet, QVector<QString>& vecSQL, QHash< QString, QString >& mapIDtoSQL )
        {
            QVector< QString > nonOrderSQL;

            for( pugi::xpath_node_set::const_iterator it = xpathNodeSet.begin(); it != xpathNodeSet.end(); ++it )
            {
                pugi::xml_node xmlNode = it->node();
                if( xmlNode.empty() == true )
                    continue;

                QString query = QString::fromStdString( xmlNode.text().as_string() ).trimmed();
                if( query.isEmpty() == true ||
                    query.compare( "(NULL)", Qt::CaseInsensitive ) == 0 )
                    continue;

                pugi::xml_attribute attOrder = xmlNode.attribute( "order" );
                if( (attOrder.empty() == false) && (strlen( attOrder.as_string() ) > 0) )   // order 를 지정하지 않거나, order 에 빈문자열을 할당하는 경우 제외
                {
                    unsigned int order = attOrder.as_uint();

                    if( vecSQL.size() <= (order + 1) )    // ORDER 는 0번부터 시작될 수 있으므로 1개 넉넉히 준비한다
                        vecSQL.resize( order + 1 );

                    vecSQL[order] = query;
                }
                else
                {
                    nonOrderSQL.push_back( query );
                }

                // schema rev 4
                pugi::xml_attribute attId = xmlNode.attribute( "id" );
                if( attId.empty() == false )
                {
                    QString id = QString::fromStdString( attId.as_string() );
                    if( id.isEmpty() == false )
                    {
                        if( mapIDtoSQL.contains( id ) == true )
                        {
                            // TODO: id 가 중복됨, 오류 출력
                        }
                        else
                        {
                            mapIDtoSQL[id] = query;
                        }
                    }
                }
            }

            if( nonOrderSQL.empty() == false )
            {
                for( int idx = 0; idx < nonOrderSQL.size(); ++idx )
                    vecSQL.push_back( nonOrderSQL[idx] );
            }
        }

        void CCmnDBMgr::loadMigrationSQLFromXML( const pugi::xpath_node_set& xpathNodeSet, QHash< unsigned int, QVector< QString > >& mapTargetToSQLs )
        {
            for( pugi::xpath_node_set::const_iterator it = xpathNodeSet.begin(); it != xpathNodeSet.end(); ++it )
            {
                pugi::xml_node xmlNode = it->node();
                if( xmlNode.empty() == true )
                    continue;

                unsigned int targetRev = xmlNode.attribute( "targetRev" ).as_uint();
                if( targetRev <= 0 )
                    continue;       // 마이그레이션 목표 리비전이 0과 같거나 작을 수 없음

                QVector< QString > vecSQLs;
                for( pugi::xml_node xmlSQLNode = xmlNode.first_child(); xmlSQLNode; xmlSQLNode = xmlSQLNode.next_sibling() )
                {
                    unsigned int order = xmlSQLNode.attribute( "order" ).as_uint();
                    if( vecSQLs.size() <= (order + 1) )
                        vecSQLs.resize( order + 1 );

                    vecSQLs[order] = QString::fromStdString( xmlSQLNode.text().as_string() );
                }

                mapTargetToSQLs.insert( targetRev, vecSQLs );
            }
        }

        bool CCmnDBMgr::checkSchemaTable( TySpCmnDB db, const QString& dbSchemaName )
        {
            bool isSuccess = false;
            QString query;

            do
            {
                QSqlQuery dbStmt;

                if( db->GetDBType() == "QSQLITE" )
                {
                    query = QString( "SELECT COUNT(*) AS CNT FROM sqlite_master WHERE type = 'table' AND name = '%1' ").arg( nsDetail::PARAM_SCHEMA_TABLENAME );
                    dbStmt = db->Exec( query );
                    if( dbStmt.next() == false )
                        break;

                    if( dbStmt.value( "CNT" ).toInt() == 0 )
                        break;
                }

                query = QString( "SELECT * FROM %1 WHERE %2 = '%3'").arg( nsDetail::PARAM_SCHEMA_TABLENAME ).arg( nsDetail::PARAM_SCHEMA_COL_NAME ).arg( dbSchemaName );
                // DB 테이블에서 현재 리비전을 읽어옴
                dbStmt = db->Exec( query );
                if( dbStmt.next() != false )
                    isSuccess = true;
                else if( dbStmt.lastError().type() != QSqlError::ErrorType::StatementError )
                    isSuccess = true;

            } while( false );

            return isSuccess;
        }

        bool CCmnDBMgr::createTables( TySpCmnDB db )
        {
            bool isSuccess = false;
//             LM_DEBUG_TO( cmndbmanager_logger, L"DB 초기화를 시작합니다" );

            do
            {
                nsDetail::CMN_DB_SCHEMA& dbSchemas = m_mapDBIdentityToSchema[db->GetDBIdentity()];
                TyDatabaseOptions& dbOptions = m_mapDBIdentityToOptions[db->GetDBIdentity()];

                // 스키마 생성 시작
                if( dbSchemas.sDBType != nsDetail::SCHEMA_DBTYPE_ORACLE && dbSchemas.sDBType != nsDetail::SCHEMA_DBTYPE_SQLITE )
                {
                    IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecCreateDBSQLs ) );
                }

                if( dbOptions.sDBType != "QSQLITE" )
                {
                    // 사용자 생성 및 생성된 사용자로 전환
//                     pugi::xml_node xmlUserNode = m_xmlDocument.select_single_node( "//createUser" ).node();
//                     if( xmlUserNode.empty() == false )
//                     {
//                         std::string username = xmlUserNode.attribute( "username" ).as_string();
//                         std::string password = xmlUserNode.attribute( "password" ).as_string();
// 
//                         bool isExistUser = false;
//                         if( stricmp( dbOptions[OPT_DB_DRIVERNAME], L"QOCI" ) == 0 )
//                         {
//                             std::wstring identifyUserSQL = tsFormat( L"SELECT USERNAME FROM DBA_USERS WHERE USERNAME = '{}'", username );
//                             QSqlQuery dbStmt = db->Exec( identifyUserSQL );
//                             if( dbStmt.lastError().type() == QSqlError::NoError )
//                             {
//                                 if( dbStmt.next() == true )
//                                 {
//                                     dbOptions[OPT_DB_ACCOUNTNAME] = tsFormat( L"{}", username );
//                                     dbOptions[OPT_DB_ACCOUNTPASS] = tsFormat( L"{}", password );
// 
//                                     LM_WARNING_TO( cmndbmanager_logger, L"해당 데이터베이스에 사용자 계정({}) 가 이미 존재하여 해당 계정을 사용합니다", username );
//                                     isExistUser = true;
//                                 }
//                             }
//                         }
// 
//                         if( isExistUser == false )
//                         {
//                             IF_FALSE_BREAK( isSuccess, createUserANDSwitchUserSchema( db,
//                                                                                       CU82U( username ).toStdString(), CU82U( password ).toStdString() )
//                             );
//                         }
// 
//                         db->Close();
//                         db = GetConnection( db->GetDBIdentity() );
//                     }
                }

                IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecCreateTables ) );
                IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecCreateFunctions ) );
                IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecCreateViews ) );
                IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecCreateIndexies ) );
                IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecPostScripts ) );
                IF_FALSE_BREAK( isSuccess, createSchemaFromXML( db, dbSchemas.vecDefaultDatas, true ) );

            } while( false );

//             LM_DEBUG_TO( cmndbmanager_logger, L"DB 초기화를 종료합니다. 결과 = {:b}", isSuccess );
            return isSuccess;
        }

        bool CCmnDBMgr::createSchemaFromXML( TySpCmnDB db, const QVector<QString>& vecSQL, bool isCommitPerSQL /*= false */ )
        {
            bool isSuccess = true;
            CCmnDBBasicTransaction transaction( db );

            do
            {
                if( isCommitPerSQL == false )
                {
                    isSuccess = transaction.begin();
                    if( isSuccess == false )
                    {
//                         LM_ERROR_TO( cmndbmanager_logger, L"스키마를 생성하기 위한 트랜잭션을 설정하는데 실패하였습니다. 오류 = {}", transaction.getLastError().text() );
                        break;
                    }
                }

                for( size_t idx = 0; idx < vecSQL.size(); ++idx )
                {
                    QString sql = vecSQL[idx];
                    if( sql.isEmpty() == true )
                        continue;

                    QSqlError lastErr = db->Exec( sql ).lastError();
                    if( lastErr.type() != QSqlError::NoError )
                    {
                        isSuccess = false;
//                         LM_ERROR_TO( cmndbmanager_logger, L"스키마를 생성하는 도중 실패하였습니다, 쿼리 = {}, 오류 = {}", sql, lastErr.text() );
                        break;
                    }

                }

                if( isSuccess == false )
                    break;

                isSuccess = true;
            } while( false );

            if( isCommitPerSQL == false )
            {
                if( isSuccess == true )
                    transaction.commit();
                else
                    transaction.rollback();
            }

            return isSuccess;
        }

        bool CCmnDBMgr::createUserANDSwitchUserSchema( TySpCmnDB db, const QString& username, const QString& password )
        {
            bool isSuccess = false;
            nsDetail::CMN_DB_SCHEMA& dbSchemas = m_mapDBIdentityToSchema[db->GetDBIdentity()];
            TyDatabaseOptions& dbOptions = m_mapDBIdentityToOptions[db->GetDBIdentity()];
            CCmnDBTransactionGuard< CCmnDBImmediateTransaction > transaction( db );

            do
            {
                isSuccess = true;
                for( size_t idx = 0; idx < dbSchemas.vecCreateUserSQLs.size(); ++idx )
                {
                    QString sql = dbSchemas.vecCreateUserSQLs[idx];

                    QSqlError lastErr = db->Exec( sql ).lastError();
                    if( lastErr.type() != QSqlError::NoError )
                    {
                        isSuccess = false;
//                         LM_ERROR_TO( cmndbmanager_logger, L"사용자 계정을 생성하는 도중 실패, 쿼리 = {}, 오류 = {}", sql, lastErr.text() );
                        break;
                    }
                }

            } while( false );

            if( isSuccess == true )
            {
                transaction.commit();

                dbOptions.sAccountName = username;
                dbOptions.sAccountPass = password;
            }
            else
            {
                transaction.rollback();
            }

            return isSuccess;
        }

        bool CCmnDBMgr::processMigrationSchemaFromXML( TySpCmnDB db, const nsDetail::TyMapRevToSQLs& mapTargetRevToSQLs, int currentDBRev )
        {
            bool isSuccess = true;

            nsDetail::CMN_DB_SCHEMA& dbSchemas = m_mapDBIdentityToSchema[db->GetDBIdentity()];
            TyDatabaseOptions& dbOptions = m_mapDBIdentityToOptions[db->GetDBIdentity()];

            unsigned int originalSchemaRev = currentDBRev;
            unsigned int currentSchemaRev = originalSchemaRev;
            CCmnDBTransactionGuard< CCmnDBImmediateTransaction > dbTransactionMgr( db );

            do
            {
                std::vector< unsigned int > vecTargetRev;
                for( nsDetail::TyMapRevToSQLs::const_iterator it = mapTargetRevToSQLs.begin();
                     it != mapTargetRevToSQLs.end(); ++it )
                    vecTargetRev.push_back( it.key() );
                std::sort( vecTargetRev.begin(), vecTargetRev.end() );

                for( std::vector< unsigned int >::iterator currentRev = vecTargetRev.begin(); currentRev != vecTargetRev.end(); ++currentRev )
                {
                    if( currentSchemaRev >= *currentRev )
                        continue;

                    if( isSuccess == false )
                        break;

                    QVector<QString> vecSQL = mapTargetRevToSQLs.find( *currentRev ).value();

//                     LM_DEBUG_TO( cmndbmanager_logger, L"스키마 리비전 {} => {} 로 마이그레이션을 진행합니다", currentSchemaRev, *currentRev );
                    for( size_t idx = 0; idx < vecSQL.size(); ++idx )
                    {
                        QString sql = vecSQL[idx];
                        if( sql.isEmpty() == true )
                            continue;

//                         LM_DEBUG_TO( cmndbmanager_logger, L"마이그레이션 쿼리 = {}", sql );

                        QSqlError lastErr = db->Exec( sql ).lastError();
                        if( lastErr.type() != QSqlError::NoError )
                        {
                            isSuccess = false;
//                             LM_ERROR_TO( cmndbmanager_logger, L"스키마를 생성하는 도중 실패하였습니다, 쿼리 = {}, 오류 = {}", sql, lastErr.text() );
                            break;
                        }
                    }

                    if( isSuccess == false )
                        break;

//                     LM_DEBUG_TO( cmndbmanager_logger, L"스키마 리비전 {} => {} 로 마이그레이션이 완료되었습니다", currentSchemaRev, *currentRev );

                    currentSchemaRev = *currentRev;
                }

                if( isSuccess == false )
                    break;

                isSuccess = true;
            } while( false );

            if( isSuccess == true )
            {
                if( ( isSuccess = dbTransactionMgr.commit() ) == true )
                    dbOptions.mapExtraKVPairs[nsDetail::PARAM_CURRENT_SCHEMA_REV] = QString("%1").arg( currentSchemaRev );
            }

            if( isSuccess == false )
            {
                dbTransactionMgr.rollback();
                dbOptions.mapExtraKVPairs[nsDetail::PARAM_CURRENT_SCHEMA_REV] = QString( "%1" ).arg( originalSchemaRev );
            }

            return isSuccess;
        }

        bool CCmnDBMgr::writeSchemaRevTodB( TySpCmnDB db, const nsDetail::CMN_DB_SCHEMA& dbSchema, bool isSuccessToInit )
        {
            QString sQuery;
            CCmnDBTransactionGuard< CCmnDBImmediateTransaction > dbTransactionMgr( db );

            do
            {
                QSqlQuery dbStmt( *db );

                if( dbSchema.sDBType.compare( nsDetail::SCHEMA_DBTYPE_SQLITE, Qt::CaseInsensitive ) == 0 )
                {
                    sQuery = SQL_SCHEMA_TABLE_DDL_SQLITE.arg( PARAM_SCHEMA_TABLENAME ).arg( PARAM_SCHEMA_COL_NAME ).arg( PARAM_SCHEMA_COL_REV ).arg( PARAM_SCHEMA_COL_ISINIT );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );

                    sQuery = QString( "INSERT OR REPLACE INTO %1 ( %2, %3, %4 ) VALUES ( '%5', %6, %7 )" )
                                      .arg( PARAM_SCHEMA_TABLENAME )
                                      .arg( PARAM_SCHEMA_COL_NAME ).arg( PARAM_SCHEMA_COL_REV ).arg( PARAM_SCHEMA_COL_ISINIT )
                                      .arg( dbSchema.sSchemaName ).arg( dbSchema.schemaRev ).arg( isSuccessToInit );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );
                }

                else if( dbSchema.sDBType.compare( nsDetail::SCHEMA_DBTYPE_MSSQL, Qt::CaseInsensitive ) == 0 )
                {
                    sQuery = SQL_SCHEMA_TABLE_DDL_MSSQL.arg( PARAM_SCHEMA_TABLENAME ).arg( PARAM_SCHEMA_COL_NAME ).arg( PARAM_SCHEMA_COL_REV ).arg( PARAM_SCHEMA_COL_ISINIT );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );

                    sQuery = QString( "IF EXISTS ( SELECT * FROM %1 WHERE %2 = N'%5' ) "
                                      "UPDATE %1 SET %3 = %6, %4 = %7 "
                                      "ELSE "
                                      "INSERT INTO %1 ( %2, %3, %4 ) VALUES ( N'%5', '%6', '%7' )" )
                                      .arg( PARAM_SCHEMA_TABLENAME)
                                      .arg( PARAM_SCHEMA_COL_NAME).arg( PARAM_SCHEMA_COL_REV).arg( PARAM_SCHEMA_COL_ISINIT)
                                      .arg( dbSchema.sSchemaName ).arg( dbSchema.schemaRev).arg( isSuccessToInit );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );
                }
                else if( dbSchema.sDBType.compare( nsDetail::SCHEMA_DBTYPE_ORACLE, Qt::CaseInsensitive ) == 0 )
                {
                    sQuery = SQL_SCHEMA_TABLE_DDL_ORACLE.arg( PARAM_SCHEMA_TABLENAME ).arg( PARAM_SCHEMA_COL_NAME ).arg( PARAM_SCHEMA_COL_REV ).arg( PARAM_SCHEMA_COL_ISINIT );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );

                    sQuery = QString( "INSERT INTO %1 ( %2, %3, %4 ) SELECT '%5', '%6', '%7' FROM DUAL WHERE NOT EXISTS ( SELECT %2 FROM %1 WHERE %2 = '%5' )" )
                                      .arg( PARAM_SCHEMA_TABLENAME )
                                      .arg( PARAM_SCHEMA_COL_NAME ).arg( PARAM_SCHEMA_COL_REV ).arg( PARAM_SCHEMA_COL_ISINIT )
                                      .arg( dbSchema.sSchemaName ).arg( dbSchema.schemaRev ).arg( isSuccessToInit );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );

                    sQuery = QString( "UPDATE %1 SET %3 = '%6', %4 = '%7' WHERE %2 = '%5'")
                                        .arg( PARAM_SCHEMA_TABLENAME)
                                        .arg( PARAM_SCHEMA_COL_NAME).arg( PARAM_SCHEMA_COL_REV).arg( PARAM_SCHEMA_COL_ISINIT)
                                        .arg( dbSchema.sSchemaName).arg( dbSchema.schemaRev).arg( isSuccessToInit );
                    IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
                    IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );

                } // if

            } while( false );

            return dbTransactionMgr.apply();
        }

    } // nsCmnDB
} // nsCmn
