﻿#ifndef __HDR_CMN_FORMATTER__
#define __HDR_CMN_FORMATTER__

#include <type_traits>

#include "cmnBase.hpp"

#include "tek/tek.h"

#include "cmnFormatterBase.hpp"
#include "cmnFormatterParse.hpp"
#include "cmnFormatterWriter.hpp"
#include "cmnFormatterUnknown.hpp"

/*!
    replacement_field       ::= "{" [arg_index] [":" format_spec ] "}"
    arg_index               ::= integer | special_arg
    special_arg             ::= "TID" | "PID"

    format_spec             ::= [[fill]align][sign]["#"]["0"][width]["." precision][type]
    fill                    ::=  <a character other than '{' or '}'>
    align                   ::=  "<" | ">" | "=" | "^"
    sign                    ::=  "+" | "-" | " "
    width                   ::=  integer | "{" arg_id "}"
    precision               ::=  integer | "{" arg_id "}"
    type                    ::=  int_type | "c" | "e" | "E" | "f" | "F" | "g" | "G" | "p" | "s" | custom_type
    int_type                ::=  "b" | "B" | "d" | "o" | "x" | "X"
    custom_type             ::=  "DT" | "ERR" | "HERR" | "STRERR" => 모두 문자열로 취급

    TAG Dispatching 및 Template Function Overloading 활용

    BasicFormatter 의 operator() 에서 formatValue 를 호출한다. 
        이때 Type 이 맞는 함수가 있다면 즉시 호출된다. ( 템플릿인자는 적게 사용되는 쪽으로 우선 선택되는 원칙 )
        맞는 함수가 없다면 nsDetail 의 formatValue 함수가 호출된다. 
        해당 함수는 T 에 대한 타입 태그를 추출하여 태그 디스패칭을 시도한다. 
        맞는 태그 형식이 없다면 unknown type 으로 처리된다. 

    int_type
        b   bool 값을 입력받아, "true", "false" 문자열
        B   bool 값을 입력받아, "TRUE", "FALSE" 문자열
        d   10진수
        o   8진수
        x   16진수 소문자
        X   16진수 대문자

    custom_type
        DT[ ":" argIndex ]
            입력받은 정수 값을 time_t 형식으로 인식하거나 struct tm 값을 받아 날짜/시간 문자열로 서식화
            argIndex, 사용할 서식화할 문자열 지정, 생략하면 기본 서식화 문자열 사용
        ERR
            입력받은 정수 값을 GetLastError() 값으로 인식하여 오류에 대한 설명 문자열로 변환
        HERR
            입력받은 정수 값을 HRESULT 값으로 인식하여 오류에 대한 설명 문자열로 변환
        STRERR
            입력받은 정수 값을 CRT Errno 값으로 인식하여 오류에 대한 설명 문자열 변환

    History :
        2012-12-10
            %0 특수문법 TID, PID 지원 추가
            출력 형식 ERR 지원 추가
            부호없는 정수에 대해 문자열로 변환이 이루어지지 않던 문제 수정
            float, double 형에 대한 간단한 문자열 출력 지원
            알 수 없는 형식에 대하여 "Unknown Type" 이라는 문자열이 찍히도록 추가함
            tsFormatBuffer 기본 구현 완성
        2013-01-07
            로거등에서 활용할 수 있도록 개선
        2013-01-14
            로거 이름을 출력할 수 있도록 추가
        2013-01-22
            로거 스펙을 fmtSpec 으로 통합
            LOG LEVEL 를 출력할 수 있도록 추가
        2013-01-30
            cmnConverter 네임스페이스를 클래스들을 인자로 넘겼을 때 자동으로 변환되어 "Unknown Type" 이라는 문자열이 출력되지 않도록 함.
        2013-05-03
            HRESULT 반환 값에 대한 설명 문자열 생성 추가
        2014-04-XX
            MSVC 2013 이상 Variadic Template 을 지원하는 컴파일러를 위한 구현 분리
        2014-08-XX
            공통 라이브러리 리팩토리에 맞추어 네임스페이스 이름 변경
            cppformat 라이브러리를 참조하여 대대적인 개선 및 문법 변경
        2014-10-XX
            문자열 포인터 형식의 char*, wchar_t* 뿐만 아니라 char, wchar_t 형식의 문자도 인자로 넘길 수 있도록 개선

        2016-07-20
            QVariant 형식에 대한 formatValue 함수 추가

        2016-10-XX
            cppformat 라이브러리의 구조를 도입하여 속도 및 구조 개선 
            char* 대해서 UTF-8 로 취급하도록 변경, ANSI 인코딩에 대해서는 UTF-8 / UTF-16 로 변환후 tsFormat 에 넣어야함
            %0 특수문법에 대한 변경
            STRERR 추가
            ENUM 형식을 INT 형으로 변환하지 않고 직접 넣을 수 있도록 개선
                enum { TEST_FLAG = 100 }
                기존 : tsFormat( L"{}", TEST_FLAG ) => "Unknown Type" 
                변경 : tsFormat( L"{}", TEST_FLAG ) => "100"

        2017-06-XX
            STL 컨테이너, Pair, Tuple 에 대한 지원 추가
            char**, wchar_t** 지원 추가 
                ( 기본적으로는 REG_MULTI_SZ 의 형태와 같이 NULL 로 구분되며 이중 NULL 로 끝나야한다 )
                배열의 길이를 지정하기 위해 precision 에 숫자를 직접 지정하거나 인자 색인를 지정할 수 있다. 
                단, 인자 색인은 인자보다 뒤에 와야한다. 
                예). tsFormat( L"{:.{}}", wszArgs, 2 );
*/

namespace nsCmn
{
    namespace nsCmnFormatter
    {
        namespace nsDetail
        {
            // 태그 디스패칭을 위한 함수 선언
            template<typename Writer, typename T, typename Tag>
            size_t formatValue( Writer& writer, FormatSpec& fspec, typename Writer::CharType* fmtBuffer, const T& value, Tag );

            template< typename Writer, typename T >
            size_t formatValue( Writer& writer, FormatSpec& fspec, typename Writer::CharType* fmtBuffer, const T& value,
                                typename nsTraits::enable_if_c< nsTraits::is_same< T, tek::private_impl::utils::null_type >::value >::type * = 0 )
            {
                typename type_tag< typename basic_type<T>::type >::type tag;

                return formatValue( writer, fspec, fmtBuffer, value, tag );
            }

        } // nsDetail
    } // nsCmnFormatter
} // nsCmn

#endif // __HDR_CMN_FORMATTER__