﻿#ifndef __HDR_CMN_LOGGER__
#define __HDR_CMN_LOGGER__

#include "cmnLogger_Defs.hpp"
#include "cmnLogger_Mgr.hpp"
#include "cmnLogger_Pattern.hpp"
#include "cmnLogger_Sink.hpp"

namespace nsCmn
{
    namespace nsCmnDB
    {
        namespace nsDetail
        {
            
        }   // nsDetail
    } // nsCmnDB
} // nsCmn

#endif // __HDR_CMN_LOGGER__