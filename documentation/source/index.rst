.. reST-ER documentation master file, created by
   sphinx-quickstart on Tue Sep 05 17:36:30 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to reST-ER's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   feature
   dbms 
   scintilla

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
