﻿
레퍼런스
==========

rstPAD : https://github.com/ShiraNai7/rstpad

공통 라이브러리
    
    cmnFormatter
        https://github.com/fmtlib
        https://www.codeproject.com/Articles/23304/High-Performance-Heterogeneous-Container
        https://www.codeproject.com/Articles/159910/Extremely-Efficient-Type-safe-printf-Library

    cmnLogger
        spdLog - https://github.com/gabime/spdlog

    cmnDB
        Transaction - https://github.com/majestrate/i2pcpp/blob/master/lib/i2p/sqlite3cc/transaction.h

기능
=======


UI
========

상단 : 도구모음

좌측 : Treeview( 프로젝트 구성 파일들 )

    conf.py 및 tortree 에 있는 rst 파일들


우측 : QWidget

    상단 QTabbar
        내부 프레임
        좌측 : Editor
        우측 : Preview

    tab 를 변경할 때 
        내부 프레임 교체( 코드 변경, 프리뷰 변경 )

    tabbar 와 연동된 관리자 필요함  


하단 상태표시줄



메뉴
=====
