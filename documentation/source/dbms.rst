﻿SQL
=======

DBMS 는 크게 2 가지로 분리된다. 

프로그램이 사용하는 설정 DB 와 각 문서 프로젝트 별로 생성되는 프로젝트 DB 이다. 

설정 DB
    프로그램과 같은 디렉토리에 위치한다. 

프로젝트 DB
    Sphinx 의 소스 디렉토리와 같은 디렉토리에 위치한다. 
    확장자는 reST-DB 이다


TBL_SETTINGS

Name 
Value

----------


FontName
FontSize
SphinxPath, 보통 파이선의 스크립트 폴더 지정, sphinx-quickstart 와 sphinx-build 가 포함되어있어야함


TBL_RECENT_PROJECTS




