﻿#include "stdafx.h"
#include "CProjectMgr.hpp"
#include "App.hpp"

#include "cmnDB.hpp"
#include "cmnPythonUtils.hpp"
#include "PythonBridge.h"

#include "cmnTypeDefs_Names.hpp"

using namespace nsCmn;
using namespace nsCmn::nsCmnDB;
using namespace nsCmn::nsPython;

//////////////////////////////////////////////////////////////////////////

bool CProjectMgr::Init()
{
    bool isSuccess = false;

    do 
    {
        isSuccess = true;

    } while (false);

    return isSuccess;
}

bool CProjectMgr::CreateNewProject( const QString& sProjectPath, const QString& sProjectFileName )
{
    bool isSuccess = false;
    Q_ASSERT( sProjectPath.isEmpty() == false );

    /*!
        프로젝트 생성

        #. 디렉토리가 존재하는지 확인
        #. 디렉토리에 conf.py 파일이 존재하는지 확인
        #. conf.py 파일 분석
        ##. master_doc 확인
        ##. source_suffix 확인
        ##. source_encoding 확인
        #. 프로젝트 파일 생성

        프로젝트 열기 로 이관
    */

    QString sMasterDoc;
    QStringList lstSourceSuffix = QStringList() << ".rst";
    QString sSourceEncoding = "utf-8";

    do 
    {
        QString sConfFilePath = QString( "%1/%2" ).arg( sProjectPath ).arg( SPHINX_CONF_FILENAME );
        if( QFile::exists( sConfFilePath ) == false )
        {
            // TODO: 오류 출력 및 대화상자 표시
            // Sphinx 환경 설정 파일이 없음
            break;
        }

        auto stPythonMgr = MAIN_INSTANCE->GetPythonMain();
        auto pyObjConf = stPythonMgr->ImportModuleFromFile( sConfFilePath );

        sMasterDoc = pyObjConf.GetVariable( "master_doc" ).toString();
        lstSourceSuffix = pyObjConf.GetVariable( "source_suffix" ).toStringList();
        sSourceEncoding = pyObjConf.GetVariable( "source_encoding" ).toString();

        if( sMasterDoc.isEmpty() == true )
        {
            // TODO: 오류 출력, 대표 문서 찾을 수 없음
            break;
        }

        QString sProjectDBFilePath = QString("%1/%2").arg( sProjectPath ).arg( sProjectFileName );
        if( sProjectDBFilePath.endsWith( APP_PROJECTDB_FILEEXT, Qt::CaseInsensitive ) == false )
            sProjectDBFilePath += "." + APP_PROJECTDB_FILEEXT;

        isSuccess = createDB( sProjectDBFilePath );
        if( isSuccess == false )
        {
            // TODO: 오류 출력
            break;
        }

        // 기본 데이터 기록
        SetProjectInfo( "master_doc", sMasterDoc );
        SetProjectInfo( "source_suffix", lstSourceSuffix.join("|") );
        SetProjectInfo( "source_encoding", sSourceEncoding );

    } while (false);

    return isSuccess;
}

void CProjectMgr::CurrentFileSave()
{
    // TODO: 파일 저장
    // TODO: Sphinx 실행, 프리뷰
    auto stCodeViewMgr = MAIN_INSTANCE->GetCodeViewMgr();



}

void CProjectMgr::SetCurrentFile( const QString& sCurrentFileName )
{
    _sCurrentFileName = sCurrentFileName;
}

QString CProjectMgr::GetCurrentFile()
{
    return _sCurrentFileName;
}

bool CProjectMgr::SetProjectInfo( const QString& sName, const QVariant& sValue )
{
    auto stCmnDBMgr = TyStCmnDBMgr::GetInstance();
    auto db = stCmnDBMgr->GetConnection( DBMS_PROJECTDB_IDEN );
    CCmnDBTransactionGuard< CCmnDBImmediateTransaction > dbTransactionMgr( db );

    do 
    {
        QSqlQuery dbStmt( *db );

        QString sQuery = "INSERT OR REPLACE INTO TBL_CONF ( NAME, VALUE ) VALUES ( :NAME, :VALUE )";
        IF_FALSE_BREAK( dbTransactionMgr, db->Prepare( dbStmt, sQuery ) );
        dbStmt.bindValue( ":NAME", sName );
        dbStmt.bindValue( ":VALUE", sValue );
        IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );

    } while (false);

    return dbTransactionMgr.apply();
}

QString CProjectMgr::GetProjectPath()
{
    // TODO:

    return "";
}

QString CProjectMgr::GetProjectBuildPath()
{
    // TODO:

    return "";
}

bool CProjectMgr::RunSphinxBuild( const QString& sSphinxPath, const QString& sBuildPath )
{
    bool isSuccess = false;

    do 
    {
        isSuccess = true;

    } while (false);

    return isSuccess;
}

bool CProjectMgr::createDB( const QString& sProjectDBFilePath )
{
    bool isSuccess = false;
    auto stCmnDBMgr = TyStCmnDBMgr::GetInstance();

    TyDatabaseOptions dbOptions;

    do 
    {
        dbOptions.sDBIdentity = DBMS_PROJECTDB_IDEN;
        dbOptions.sDBName = sProjectDBFilePath;
        dbOptions.sDBType = "QSQLITE";
        dbOptions.sSchemaFileName = DBMS_PROJECTDB_SCHEMA;
        
        isSuccess = stCmnDBMgr->InitDBMS( dbOptions );
        
    } while (false);

    return isSuccess;
}

QString CProjectMgr::unquoteString( const QString& sText )
{
    QString sRet;
    
    for( auto ch : sText )
    {
        if( ch == '\'' || ch == '\"' )
            continue;

        sRet += ch;
    }

    return sRet;
}
