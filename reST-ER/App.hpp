﻿#ifndef __HDR_APP_MAIN__
#define __HDR_APP_MAIN__

#include <QApplication>

#define MAIN_INSTANCE (static_cast<CMain*>(QCoreApplication::instance()))

class reSTER;
class CProjectMgr;
class CSettingsMgr;
class CCodeViewMgr;

namespace RstPad
{
    class PythonBridge;
}

namespace nsCmn
{
    namespace nsPython
    {
        class CPyMain;
    }
}

class CMain : public QApplication
{
public:
    CMain( int &argc, char **argv );
    virtual ~CMain();

    static int exec();

    bool                                            Initialize();

    nsCmn::nsPython::CPyMain*                       GetPythonMain();
    CSettingsMgr*                                   GetSettingsMgr();
    CCodeViewMgr*                                   GetCodeViewMgr();
    CProjectMgr*                                    GetProjectMgr();

private:

    nsCmn::nsPython::CPyMain*                       m_rpPythonMain;
    RstPad::PythonBridge*                           m_rpPythonBridge;

    reSTER*                                         m_rpMainWindow;
    CProjectMgr*                                    m_rpProjectMgr;
    CSettingsMgr*                                   m_rpSettingsMgr;
    CCodeViewMgr*                                   m_rpCodeViewMgr;

    int _argc;
    char **_argv;
};

class CCurrentState
{

    /*!
        #. 현재 선택된 TreeView 항목
        ##. 해당 항목이 rst 인지 여부

        #. 탭 위젯 - 색인 / View
        
    */


};

#endif // __HDR_APP_MAIN__