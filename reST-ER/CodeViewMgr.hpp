﻿#ifndef __HDR_APP_CODEVIEW_MGR__
#define __HDR_APP_CODEVIEW_MGR__

#include <QtCore>
#include <QtGui>

/*!
    TabWidget 및 내부의 Code Editor, Preview Widget 등을 관리하는 클래스

    http://www.scintilla.org/ScintillaDoc.html
*/

class QTabWidget;
class CView;

class CCodeViewMgr : public QObject
{
    Q_OBJECT
public:

    CCodeViewMgr();
    CCodeViewMgr( QTabWidget* tabWidget );
    ~CCodeViewMgr();

    void                                            SetTabWidget( QTabWidget* tabWidget );

public slots:

    void                                            InitUIs();

    // 뷰를 추가하고, 탭 색인번호를 반환합니다. 지정한 문자열로 탭의 제목을 지정합니다. 
    // 맵에 자동으로 추가합니다.
    int                                             AddView( const QString& sTabTitle = "" );
    // CodeView 의 현재 탭을 전환합니다
    void                                            SwitchToView( int index );
    void                                            ClearViews();

    //////////////////////////////////////////////////////////////////////////
    /// Scintill 관리

    // 배경색상 변경
    // 글자색상 변경
    // 편집 - 되돌리기
    // 편집 - 복사
    // 편집 - 붙여넣기
    // 편집 - 잘라내기
    // 편집 - 모두 선택

    // 특정 탭의 문서 내용 가져오기
    
    //////////////////////////////////////////////////////////////////////////
    /// 탭 관리

    // 탭에 수정 상태 표시/해제, ( 탭 제목에 * 붙이기 )

    //////////////////////////////////////////////////////////////////////////
    /// 프리뷰 관리

    // .rst 파일이름을 전달하여, 해당 하는 빌드 파일을 찾은 후 표시한다. 
    void                                            Preview( const QString& sFileName );

protected slots:
    // 사용자가 탭에서 닫기 버튼을 클릭했을 때
    void                                            OnTabClose( int index );
    void                                            OnTabChanged( int index );

private:

    // key = tab index, value = CView Ptr
    typedef QHash< int, CView* > TyMapTabIndexToViewUI;

    QTabWidget*                                     _tabWidget;
    TyMapTabIndexToViewUI                           _mapTabIndexToViewUI;
};

#endif // __HDR_APP_CODEVIEW_MGR__