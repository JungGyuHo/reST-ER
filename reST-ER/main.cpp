﻿#include "stdafx.h"
#include <QtWidgets/QApplication>

#include "App.hpp"

int main(int argc, char *argv[])
{
    int nRet = 0;
    QApplication::setQuitOnLastWindowClosed( true );

    do 
    {
        CMain app( argc, argv );

        nRet = app.exec();

    } while (false);

    return nRet;
}
