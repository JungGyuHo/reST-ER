﻿#ifndef __HDR_APP_SETTINGS_MGR__
#define __HDR_APP_SETTINGS_MGR__

#include "Uis/Settings.hpp"

#include <QtCore>

class CSettingsUI;

class CSettingsMgr : public QObject
{
    Q_OBJECT
public:

    CSettingsMgr();

    bool                                            Init();

    void                                            RefreshFromDB();

    QString                                         GetSphinxPath();
    void                                            SetSphinxPath( const QString& sSphinxPath );

    void                                            ShowSettingsUI();

    // 가장 마지막으로 접근한 폴더 경로 ( New/Open )
    QString                                         GetRecentOpenPath();

protected slots:
    void                                            onSettingsChanged( const QVector< POLICY >& vecChgPolicies );

private:
    
    // Sphinx 가 설치된 경로, 마지막에는 경로 구분자가 붙지 않음
    QString                                         _sSphinxPath;
};

#endif // __HDR_APP_SETTINGS_MGR__