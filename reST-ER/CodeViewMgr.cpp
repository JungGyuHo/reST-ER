﻿#include "stdafx.h"
#include "CodeViewMgr.hpp"

#include "View.hpp"

//////////////////////////////////////////////////////////////////////////

CCodeViewMgr::CCodeViewMgr()
    : _tabWidget(NULLPTR)
{

}

CCodeViewMgr::CCodeViewMgr( QTabWidget* tabWidget )
    : _tabWidget( tabWidget )
{
    Q_ASSERT( _tabWidget != NULLPTR );
    
    _tabWidget->clear();
    SetTabWidget( tabWidget );
}

CCodeViewMgr::~CCodeViewMgr()
{
    ClearViews();
}

void CCodeViewMgr::SetTabWidget( QTabWidget* tabWidget )
{
    Q_ASSERT( tabWidget != NULLPTR );
    _tabWidget = tabWidget;
}

void CCodeViewMgr::InitUIs()
{
    ClearViews();

    connect( _tabWidget, &QTabWidget::tabCloseRequested, this, &CCodeViewMgr::OnTabClose, Qt::UniqueConnection );
    connect( _tabWidget, &QTabWidget::currentChanged, this, &CCodeViewMgr::OnTabChanged, Qt::UniqueConnection );

    AddView();
}

int CCodeViewMgr::AddView( const QString& sTabTitle )
{
    int index = -1;

    do 
    {
        CView* view = new CView( _tabWidget );
        Q_CHECK_PTR( view );
        if( view == NULLPTR )
            break;

        view->InitUIs();

        index = _tabWidget->addTab( view, sTabTitle.isEmpty() == true ? tr( "제목없음" ) : sTabTitle );
        _mapTabIndexToViewUI.insert( index, view );

    } while (false);

    return index;
}

void CCodeViewMgr::SwitchToView( int index )
{
    Q_ASSERT( index >= 0 );
    Q_CHECK_PTR( _tabWidget );

    _tabWidget->setCurrentIndex( index );
}

void CCodeViewMgr::ClearViews()
{
    Q_CHECK_PTR( _tabWidget );

    _tabWidget->clear();
    
    for( auto view : _mapTabIndexToViewUI )
    {
        if( view != NULLPTR )
            delete view;
    }

    _mapTabIndexToViewUI.clear();
}

void CCodeViewMgr::Preview( const QString& sFileName )
{

}

void CCodeViewMgr::OnTabClose( int index )
{
    // TODO:
    /*!
        #. 파일을 저장되었는지 확인( 수정 상태 )
        #. 저장되지 않았다면 확인창 표시
        #. 뷰 제거
    */

}

void CCodeViewMgr::OnTabChanged( int index )
{
    // TODO: ProjectMgr 에게 파일 변경 알림

    if( index == -1 )
    {

    }
    else if( index >= 0 )
    {

    }
}
