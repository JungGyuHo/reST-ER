﻿#include "stdafx.h"
#include "App.hpp"

#include "MainWindow.hpp"
#include "CProjectMgr.hpp"
#include "CSettingsMgr.hpp"
#include "CodeViewMgr.hpp"

#include "cmnMainDBMgr.hpp"

#include "PythonBridge.h"
#include "cmnPythonUtils.hpp"

#include <QtWebEngineWidgets>

//////////////////////////////////////////////////////////////////////////

CMain::CMain( int &argc, char **argv )
    : QApplication( argc, argv ), _argc(argc), _argv(argv)

    , m_rpPythonMain(NULLPTR)
    , m_rpPythonBridge( NULLPTR )

    , m_rpMainWindow(NULLPTR)
    , m_rpProjectMgr(NULLPTR)
    , m_rpSettingsMgr(NULLPTR)
    , m_rpCodeViewMgr(NULLPTR)
{
}

CMain::~CMain()
{
    if( m_rpPythonMain != NULLPTR )
        delete m_rpPythonMain;

    if( m_rpPythonBridge != NULLPTR )
        delete m_rpPythonBridge;

    if( m_rpSettingsMgr != NULLPTR )
        delete m_rpSettingsMgr;

    if( m_rpCodeViewMgr != NULLPTR )
        delete m_rpCodeViewMgr;

    if( m_rpMainWindow != NULLPTR )
        delete m_rpMainWindow;

    if( m_rpProjectMgr != NULLPTR )
        delete m_rpProjectMgr;
}

int CMain::exec()
{
    auto stMain = static_cast<CMain*>(qApp);

    // TODO: 로그 출력하도록 변경
    if( stMain->Initialize() == false )
        return -1;

    return QApplication::exec();
}

bool CMain::Initialize()
{
    bool isSuccess = false;

    do 
    {
        auto stMainDBMgr = TyStMainDBMgr::GetInstance();
        if( stMainDBMgr->Init() == false )
            break;

        m_rpPythonMain = new nsCmn::nsPython::CPyMain( "reST-ER" );

        m_rpPythonMain->AppendPath( applicationDirPath().append( "." ) );
        m_rpPythonMain->AppendPath( applicationDirPath().append( "/libs" ) );
        m_rpPythonMain->AppendPath( applicationDirPath().append( "/libs/libs.zip" ) );

        auto defaultWebProfile = QWebEngineProfile::defaultProfile();
        Q_CHECK_PTR( defaultWebProfile );
        defaultWebProfile->setCachePath( applicationDirPath().append("/cache") );
        defaultWebProfile->setPersistentStoragePath( applicationDirPath().append( "/cache" ) );
        defaultWebProfile->setHttpCacheType( QWebEngineProfile::MemoryHttpCache );

        //////////////////////////////////////////////////////////////////////////
        /// 설정 가져오기

        m_rpProjectMgr = new CProjectMgr;
        if( m_rpProjectMgr->Init() == false )
            break;

        m_rpSettingsMgr = new CSettingsMgr;
        if( m_rpSettingsMgr->Init() == false )
            break;

        //////////////////////////////////////////////////////////////////////////
        ///

        //////////////////////////////////////////////////////////////////////////
        /// 솔루션 탐색기 초기화

        //////////////////////////////////////////////////////////////////////////
        /// 뷰 초기화

        m_rpCodeViewMgr = new CCodeViewMgr;

        //////////////////////////////////////////////////////////////////////////
        /// 

        m_rpMainWindow = new reSTER;
        m_rpMainWindow->show();

        isSuccess = true;

    } while (false);

    return isSuccess;
}

nsCmn::nsPython::CPyMain* CMain::GetPythonMain()
{
    return m_rpPythonMain;
}

CSettingsMgr* CMain::GetSettingsMgr()
{
    return m_rpSettingsMgr;
}

CCodeViewMgr* CMain::GetCodeViewMgr()
{
    return m_rpCodeViewMgr;
}

CProjectMgr* CMain::GetProjectMgr()
{
    return m_rpProjectMgr;
}

/*!

*/

