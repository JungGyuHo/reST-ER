﻿#ifndef __HDR_APP_PROJECT_MGR__
#define __HDR_APP_PROJECT_MGR__

#include <QtCore>

#include "cmnBase.hpp"

/*!
    프로젝트 관리 클래스

    https://github.com/vitaut-archive/rstparser
*/

const QString DBMS_PROJECTDB_IDEN = "projectDB-Identity";
const QString DBMS_PROJECTDB_SCHEMA = ":/dbms/Resources/reST-ER-Project.xml";

class CProjectMgr : public QObject
{
    Q_OBJECT
public:

    bool                                            Init();


public slots:

    // 프로젝트 생성
    bool                                            CreateNewProject( const QString& sProjectPath, const QString& sProjectFileName );

    // 프로젝트 열기, signal : 프로젝트 열기 완료
    // 프로젝트 닫기

    // 파일 추가
    // 문서 파일 목록 가져오기
    // 파일 저장하기

    void                                            CurrentFileSave();
    
    void                                            SetCurrentFile( const QString& sCurrentFileName );
    QString                                         GetCurrentFile();

    // Sphinx-Build
    // 해당 파일에 일치하는 html 파일 찾기
    // 해당 파일 프리뷰에 연결

    bool                                            SetProjectInfo( const QString& sName, const QVariant& sValue );


    QString                                         GetProjectPath();
    QString                                         GetProjectBuildPath();

    //////////////////////////////////////////////////////////////////////////
    /// Sphinx

    // 현재 프로젝트를 빌드합니다. 
    bool                                            RunSphinxBuild( const QString& sSphinxPath, const QString& sBuildPath );

private:
    bool                                            createDB( const QString& sProjectDBFilePath );

    QString                                         unquoteString( const QString& sText );


    // 현재 프로젝트 파일 이름
    QString                                         _sCurrentFileName;
};


#endif // __HDR_APP_PROJECT_MGR__