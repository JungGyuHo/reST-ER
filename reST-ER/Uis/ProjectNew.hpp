﻿#ifndef __HDR_UI_PROJECTNEW__
#define __HDR_UI_PROJECTNEW__

#include "ui_ProjectNew.h"

class CProjectNewUI : public QDialog
{
    Q_OBJECT
public:
    CProjectNewUI( QDialog* parent = 0 );

public slots:

    QString                                         GetProjectPath() { return ui.edtProjectPath->text(); }
    QString                                         GetProjectFileName() { return ui.edtProjectFileName->text(); }

    void                                            on_btnBrowseConf_clicked( bool checked = false );

private:
    
    Ui::dlgProjectNew                               ui;
};

#endif // __HDR_UI_PROJECTNEW__