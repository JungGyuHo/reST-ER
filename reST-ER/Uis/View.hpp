﻿#ifndef __HDR_UI_VIEW__
#define __HDR_UI_VIEW__

#include "ui_ViewH.h"

#include <QWebEngineView>

#ifdef _DEBUG
#pragma comment( lib, "Qt5WebEngineWidgetsd")
#else
#pragma comment( lib, "Qt5WebEngineWidgets")
#endif

class ScintillaDocument;

class CView : public QWidget
{
    Q_OBJECT
public:
    CView( QWidget* parent = 0 );

    void                                InitUIs();

    sptr_t                              Send( unsigned int iMessage, uptr_t wParam = 0, sptr_t lParam = 0 ) const;
    sptr_t                              Sends( unsigned int iMessage, uptr_t wParam = 0, const char *s = 0 ) const;

private:

    ScintillaEdit*                      _codeEdit;
    ScintillaDocument*                  _document;

    Ui::View                            ui;
};

#endif // __HDR_UI_VIEW__