﻿#include "stdafx.h"
#include "MainWindow.hpp"

#include "App.hpp"
#include "Settings.hpp"
#include "CProjectMgr.hpp"
#include "CSettingsMgr.hpp"
#include "CodeViewMgr.hpp"

/// UIs
#include "ProjectNew.hpp"
#include "cmnTypeDefs.hpp"
#include "cmnTypeDefs_Names.hpp"

reSTER::reSTER(QWidget *parent)
    : QMainWindow(parent)
{
    qRegisterMetaType< TyEnSolutionTreeItemType >( "TyEnSolutionTreeItemType" );

    ui.setupUi(this);

    InitUIs();
}

void reSTER::InitUIs()
{
    auto stCodeViewMgr = MAIN_INSTANCE->GetCodeViewMgr();
    stCodeViewMgr->SetTabWidget( ui.tabCodeView );
    stCodeViewMgr->InitUIs();

    ui.objSolutionExplorer->setFixedWidth( 180 );

    setFixedSize( QSize( 1280, 720 ) );
}

void reSTER::on_acProjectNew_triggered( bool checked /*= false */ )
{
    // TODO: 기존 프로젝트가 진행 중이고, 저장하지 않았다면 저장 창을 띄운다. 

    CProjectNewUI dlgProjectNew;
    int ret = dlgProjectNew.exec();

    if( ret == 1 )
    {
        QString sProjectPath = dlgProjectNew.GetProjectPath();
        QString sProjectFileName = dlgProjectNew.GetProjectFileName();

        auto stProjectMgr = MAIN_INSTANCE->GetProjectMgr();
        if( stProjectMgr->CreateNewProject( sProjectPath, sProjectFileName ) == true )
        {
            ui.treProject->clear();

            auto item = new QTreeWidgetItem;
            item->setText( 0, sProjectFileName );
            item->setData( 0, SOLUTIONTREE_ITEM_TYPE_ROLE, SolutionTreeItemTop );

            ui.treProject->addTopLevelItem( item );

            auto stCodeViewMgr = MAIN_INSTANCE->GetCodeViewMgr();
            stCodeViewMgr->ClearViews();


//             int index = stCodeViewMgr->AddView();
//             stCodeViewMgr->SwitchToView( index );
        }
    }
}

void reSTER::on_acProjectOpen_triggered( bool checked /*= false */ )
{
    /*!
        #. 파일 선택 대화상자( 확장자 APP_PROJECTDB_FILEEXT )
        #. 해당 파일에 대한 유효성 확인
        #. 파일 가져오기
    */

    QString sFilter = QString( tr( "Settings ( *.%1) " ) ).arg( APP_PROJECTDB_FILEEXT );
    QString sSelectedFile = QFileDialog::getOpenFileName( this, APP_MAIN_TITLE_UI, QString(), sFilter );
    
    do 
    {
        if( sSelectedFile.isEmpty() == true )
        {
            // TODO: 파일을 선택하지 않음
            break;
        }



    } while (false);
}

void reSTER::on_acDocumentNew_triggered( bool checked /*= false */ )
{
    /*!
        TODO: 파일 추가

        #. 파일 추가 대화상자 
    */
    auto stProjectMgr = MAIN_INSTANCE->GetProjectMgr();

    QString sNewFileName = QFileDialog::getSaveFileName( this, tr( "Create New Document" ), stProjectMgr->GetProjectPath(), tr( "reStructuredText (*.rst)" ) );
    
    if( sNewFileName.isEmpty() == true )
        return;



}

void reSTER::on_acFileSave_triggered( bool checked /*= false */ )
{
    /*!
        파일 저장 후 스핑크스 빌드, 프리뷰 표시, 현재 위치로 스크롤
    */
    auto stProjectMgr = MAIN_INSTANCE->GetProjectMgr();
    auto stCodeViewMgr = MAIN_INSTANCE->GetCodeViewMgr();

    // 현재 파일 저장
    stProjectMgr->CurrentFileSave();
    stProjectMgr->RunSphinxBuild( stProjectMgr->GetProjectPath(), stProjectMgr->GetProjectBuildPath() );
    stCodeViewMgr->Preview( stProjectMgr->GetCurrentFile() );
}

void reSTER::on_acFileExit_triggered( bool checked /*= false */ )
{
    MAIN_INSTANCE->quit();
}

void reSTER::on_acSettings_triggered( bool checked /*= false */ )
{
    auto stSettingsMgr = MAIN_INSTANCE->GetSettingsMgr();

    stSettingsMgr->ShowSettingsUI();
}

void reSTER::on_acAboutIt_triggered( bool checked /*= false */ )
{

}

void reSTER::on_treProject_currentItemChanged( QTreeWidgetItem *current, QTreeWidgetItem *previous )
{
    /*!
        클릭한 파일 열기 등등
    */

    if( current == NULLPTR )
    {
        
    }
    else
    {
        // 클릭한 항목 가져오기
        // 해당 파일이 열려있는지 확인, 열려있지 않다면 탭 추가
    }
}

void reSTER::on_treProject_customContextMenuRequested( const QPoint &pos )
{

    /*!
        #. 마우스 위치를 이용하여 항목 확인하기
        #. 해당 항목에 따른 메뉴 출력
        ##. 솔루션 파일  : 새 파일 추가 
        ##. 파일         : 파일 열기, 파일 닫기     
        ##. 디렉토리     : 
        ##. 빈 공간      : 새 파일 추가
    */

    QMenu mnuContext;

    auto item = ui.treProject->itemAt( pos );
    if( item != NULLPTR )
    {
        TyEnSolutionTreeItemType eItemType = item->data( 0, SOLUTIONTREE_ITEM_TYPE_ROLE ).value<TyEnSolutionTreeItemType>();
        

    }
    else
    {
        // 빈 공간
    }

    mnuContext.exec( pos );
}
