﻿#include "stdafx.h"
#include "Settings.hpp"

CSettingsUI::CSettingsUI( QDialog* parent /*= 0 */ )
    : QDialog( parent )
{
    ui.setupUi( this );

}

void CSettingsUI::Init()
{
    // 기존 설정 값 불러오기
}

void CSettingsUI::on_btnFontBrowse_clicked( bool checked /*= false */ )
{
    QFontDialog dlgFontBrowse;

    bool isOk = false;
    QFont selectedFont = QFontDialog::getFont( &isOk, this );
    if( isOk == true )
    {
        ui.edtFontFamily->setText( selectedFont.family() );
        ui.spbFontSize->setValue( selectedFont.pointSize() );

        POLICY fontFamily( SPOL_CDE_FONTNAME );
        fontFamily.vtValue = selectedFont.family();

        POLICY fontSize( SPOL_CDE_FONTSIZE );
        fontSize.vtValue = selectedFont.pointSize();

        vecPolicies.push_back( fontFamily );
        vecPolicies.push_back( fontSize );
    }

}

void CSettingsUI::on_btnOK_clicked( bool checked /*= false */ )
{
    if( vecPolicies.isEmpty() == false )
        emit sigSettingsChanged( vecPolicies );

    done( 1 );
}

void CSettingsUI::on_btnCancel_clicked( bool checked /*= false */ )
{
    done( -1 );
}

void CSettingsUI::on_btnSelectColorBG_clicked( bool checked /*= false */ )
{
    QColor clrSelectedColor = QColorDialog::getColor( Qt::black, this, tr( "편집기 배경색을 선택하세요" ) );

    // background-color: rgb(0, 0, 0);
    ui.lblColorBG->setStyleSheet( QString("background-color: rgb(%1,%2,%3)").arg( clrSelectedColor.red() ).arg( clrSelectedColor.green() ).arg( clrSelectedColor.blue() ) );

    POLICY color( SPOL_CDE_BACKGROUND_COLOR );
    color.vtValue = clrSelectedColor.name();

    vecPolicies.push_back( color );
}
