﻿#ifndef __HDR_UI_SETTINGS__
#define __HDR_UI_SETTINGS__

#include <QtGui>

#include "ui_Settings.h"

#include "cmnPolicyDefs.hpp"

/*!
    변경된 값 들에 대한 목록을 저장하고 있다가, 창이 닫힐 때 해당 값을 설정관리자에게 전송한다. 
*/

class CSettingsUI : public QDialog
{
    Q_OBJECT
public:
    CSettingsUI( QDialog* parent = 0 );

    void                                            Init();

public slots:

    void                                            on_btnFontBrowse_clicked( bool checked = false );
    void                                            on_btnOK_clicked( bool checked = false );
    void                                            on_btnCancel_clicked( bool checked = false );
    void                                            on_btnSelectColorBG_clicked( bool checked = false );

signals:

    void                                            sigSettingsChanged( const QVector< POLICY >& vecChgPolicies );

private:

    QVector< POLICY >                               vecPolicies;

    Ui::dlgSettings                                 ui;

};

#endif // __HDR_UI_SETTINGS__