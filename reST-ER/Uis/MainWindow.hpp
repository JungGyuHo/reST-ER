﻿#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"

class reSTER : public QMainWindow
{
    Q_OBJECT

public:
    reSTER(QWidget *parent = Q_NULLPTR);

public slots:
    void                                            InitUIs();

    //////////////////////////////////////////////////////////////////////////
    ///

    void                                            on_acProjectNew_triggered( bool checked = false );
    void                                            on_acProjectOpen_triggered( bool checked = false );
    void                                            on_acDocumentNew_triggered( bool checked = false );

    void                                            on_acFileSave_triggered( bool checked = false );
    void                                            on_acFileExit_triggered( bool checked = false );

    void                                            on_acSettings_triggered( bool checked = false );

    void                                            on_acAboutIt_triggered( bool checked = false );

    void                                            on_treProject_currentItemChanged( QTreeWidgetItem *current, QTreeWidgetItem *previous );
    /// 솔루션 탐색기 에서 마우스 메뉴 요청
    void                                            on_treProject_customContextMenuRequested( const QPoint &pos );

private:

    //////////////////////////////////////////////////////////////////////////
    /// 솔루션 탐색기

    QAction                                         _ac;


    Ui::reSTERUI                                    ui;
};
