﻿#include "stdafx.h"
#include "ProjectNew.hpp"

#include "cmnTypeDefs_Names.hpp"

CProjectNewUI::CProjectNewUI( QDialog* parent /*= 0 */ )
    : QDialog( parent )
{
    ui.setupUi( this );
}

void CProjectNewUI::on_btnBrowseConf_clicked( bool checked /*= false */ )
{
    QString sConfFilePath = QFileDialog::getOpenFileName( this, tr( "Sphinx 설정 파일 선택" ), QString(), tr( "Sphinx Configration (conf.py)" ) );

    if( sConfFilePath.isEmpty() == false )
    {
        QFileInfo fileInfo( sConfFilePath );

        ui.edtProjectPath->setText( fileInfo.path() );
    }
}
