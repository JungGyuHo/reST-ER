﻿#include "stdafx.h"
#include "View.hpp"

#include "ScintillaEdit.h"
#include "ScintillaDocument.h"


CView::CView( QWidget* parent /*= 0 */ )
    : QWidget( parent )
    , _document(NULLPTR)
    , _codeEdit(NULLPTR)
{
    ui.setupUi( this );

    _codeEdit = ui.codeEditor;
//     _document = new ScintillaDocument( _codeEdit );
    
}

void CView::InitUIs()
{
    ui.splView->setSizes( QList<int>() << 200 << 200 );

    _codeEdit->send( SCI_SETTABWIDTH, 4, 0 );
    _codeEdit->send( SCI_SETFONTQUALITY, SC_EFF_QUALITY_ANTIALIASED, 0 );
    
    
//     _document = new ScintillaDocument( _codeEdit, (void*)_codeEdit->send( SCI_GETDOCPOINTER, 0, 0 ) );
//     
//     ;

//     _codeEdit->send( SCI_CREATEDOCUMENT, 0, 0 );

//     _codeEdit->send( SCI_SETTEXT, 0, (sptr_t)"fdsfdsfdsewrew" );
//     _codeEdit->send( SCI_SELECTALL, 0, 0 );
//     _codeEdit->send( SCI_COPY, 0, 0 );
//     _codeEdit->send( SCI_PASTE, 0, 0 );
// 
// 
//     _codeEdit->send( SCI_STYLESETBACK, STYLE_DEFAULT, (sptr_t)RGB(128,128,128) );
}

sptr_t CView::Send( unsigned int iMessage, uptr_t wParam /*= 0*/, sptr_t lParam /*= 0 */ ) const
{
    return _codeEdit->send( iMessage, wParam, lParam );
}

sptr_t CView::Sends( unsigned int iMessage, uptr_t wParam /*= 0*/, const char *s /*= 0 */ ) const
{
    return _codeEdit->sends( iMessage, wParam, s );
}
