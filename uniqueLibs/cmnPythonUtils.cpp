﻿#include "stdafx.h"
#include "cmnPythonUtils.hpp"

#include "Python.h"

//////////////////////////////////////////////////////////////////////////

namespace nsCmn
{
    namespace nsPython
    {
        namespace nsDetail
        {
            QAtomicInteger<bool>             IsThreadSupport( false );
            QAtomicInteger<bool>             IsThreadSupportInitialized( false );

            QAtomicInteger<bool>             IsUseMultiInterpreter( false );
        }

        //////////////////////////////////////////////////////////////////////////

        CPythonModule::CPythonModule( const char* szProgamName /* = "" */, const QStringList& lstLibsPaths /* = QStringList()<<"/libs" */ )
            : _pyModule(NULLPTR)
            , _rpThreadState( NULLPTR )
        {
            Py_SetProgramName( const_cast<char*>(szProgamName) );

//             _isOwnThreadInitialized = PyEval_ThreadsInitialized();
//             if( _isOwnThreadInitialized == 0 )
//                 PyEval_InitThreads();
            
            _isOwnInitialized = Py_IsInitialized();
            if( _isOwnInitialized == 0 )
                Py_Initialize();

//             _rpThreadState = Py_NewInterpreter();
//             PyThreadState_Swap( _rpThreadState );

            
            QStringList pythonLibs;
            pythonLibs.append( QApplication::applicationDirPath().append( "/libs" ) );
            pythonLibs.append( QApplication::applicationDirPath().append( "/libs/libs.zip" ) );

            initializePythonEnvironment( pythonLibs );
        }

        CPythonModule::~CPythonModule()
        {
            if( _pyModule != NULLPTR )
                Py_XDECREF( _pyModule );
            _pyModule = NULLPTR;

//             PyThreadState_Swap( NULL );
//             if( _rpThreadState != NULLPTR )
//                 Py_EndInterpreter( _rpThreadState );
// 
            if( _isOwnInitialized == 0  )
                Py_Finalize();

        }

        void CPythonModule::eval( const char* szCode, const QVariantMap& mapVtLocalParams, const char* szFileNameInBackTrace /*= __FILE__ */ )
        {
            CPyGILStateRAII pyThreadRAII;

            if( _pyModule != NULLPTR )
                Py_XDECREF( _pyModule );
            _pyModule = NULLPTR;

            PyCodeObject* rpCompiledObj = (PyCodeObject*)Py_CompileString( szCode, szFileNameInBackTrace, Py_file_input );

            do 
            {
                if( rpCompiledObj == NULLPTR )
                    break;

                PyObject* rpGlobalDict = PyModule_GetDict( PyImport_AddModule( "__main__" ) );
                PyObject* rpLocalDict = mapToPythonDict( mapVtLocalParams );

                auto fdsfds = PyString_FromString( szFileNameInBackTrace );
                auto rrrr = PyImport_Import( fdsfds );


                // _pyModule = PyRun_String( szCode, Py_file_input, rpGlobalDict, rpLocalDict );
                _pyModule = PyEval_EvalCode( rpCompiledObj, rpGlobalDict, rpLocalDict );
                // PyImport_Import( rpCompiledObj );
                CPythonException exp;

                if( _pyModule != NULLPTR && rrrr != NULLPTR )
                {
                    auto rewrwe = ConvertToQVariant( _pyModule );
                    auto rewrwe21 = ConvertToQString( _pyModule );

                    auto frwe = GetVariable( rpGlobalDict, "master_doc" );
                    auto frwe3 = GetVariable( rpLocalDict, "master_doc" );
                    auto frwe2 = GetVariable( PyImport_AddModule( "__main__" ), "master_doc" );
                    auto fdsfds = GetVariable( "master_doc" );
                    auto frwe23 = GetVariable( rrrr, "master_doc" );

                    int a = 0;
                }

//                 Py_XDECREF( rpLocalDict );
//                 Py_XDECREF( rpCompiledObj );

                if( _pyModule == NULLPTR && PyErr_Occurred() )
                {
                    PyObject *pType, *pValue, *pTraceback;

                    // fetch python exception
                    PyErr_Fetch( &pType, &pValue, &pTraceback );
                    PyErr_NormalizeException( &pType, &pValue, &pTraceback );

                    // get type name
                    QString type;
                    auto pTypeName = PyObject_GetAttrString( pType, "__name__" );
                    if( pTypeName )
                    {
                        type = ConvertToQString( pTypeName );
                    }
                    else
                    {
                        type = QString();
                    }
                    Py_DECREF( pType );

                    // get message
                    QString message;
                    if( pValue )
                    {
                        message = ConvertToQString( pValue );
                        Py_DECREF( pValue );
                    }
                    else
                    {
                        message = QString();
                    }

//                     // get trace
//                     QString trace;
//                     if( pTraceback )
//                     {
//                         auto pFormatTbArgs = PyTuple_New( 1 );
//                         PyTuple_SetItem( pFormatTbArgs, 0, pTraceback ); // steals reference to pTraceback
//                         trace = doCallModuleFunction( "traceback", "format_tb", pFormatTbArgs, Print ).toStringList().join( "" );
//                         Py_DECREF( pFormatTbArgs );
//                     }
//                     else
//                     {
//                         trace = QString();
//                     }
// 
//                     // set current exception
//                     m_currentException = new PythonException( type, message, trace );

                }

                int a = 0;


            } while (false);
        }

        QVariant CPythonModule::GetVariable( const QString& sVarName )
        {
            return GetVariable( _pyModule, sVarName );
        }

        QVariant CPythonModule::GetVariable( PyObject* object, const QString& sVarName )
        {
            QVariant vtData;
            //             Q_ASSERT( _pyModule != NULLPTR );
            //             Q_ASSERT( sVarName.isEmpty() == false );

            do
            {
                if( _pyModule == NULLPTR )
                    break;

                CPyGILStateRAII pyThreadRAII;

                PyImport_ImportModule( "__main__" );
                PyObject* objRet = PyObject_GetAttrString( object, sVarName.toUtf8() );
                if( objRet == NULLPTR )
                {

                    if( PyErr_Occurred() )
                    {
                        PyObject *pType, *pValue, *pTraceback;

                        // fetch python exception
                        PyErr_Fetch( &pType, &pValue, &pTraceback );
                        PyErr_NormalizeException( &pType, &pValue, &pTraceback );

                        // get type name
                        QString type;
                        auto pTypeName = PyObject_GetAttrString( pType, "__name__" );
                        if( pTypeName )
                        {
                            type = ConvertToQString( pTypeName );
                        }
                        else
                        {
                            type = QString();
                        }
                        Py_DECREF( pType );

                        // get message
                        QString message;
                        if( pValue )
                        {
                            message = ConvertToQString( pValue );
                            Py_DECREF( pValue );
                        }
                        else
                        {
                            message = QString();
                        }

                    }
                    break;  // TODO: 오류 생성
                }

                vtData = ConvertToQVariant( objRet );

            } while( false );

            return vtData;
        }

        void CPythonModule::initializePythonEnvironment( const QStringList &libPaths )
        {
            QVariantMap args;
            args.insert( "libPaths", libPaths );

            auto code = R"code(
import sys

sys.path = libPaths
sys.dont_write_bytecode = True
)code";

            eval( code, args );
// 
//             QString code = "\r\n";
//             code += "import sys\r\n";
//             code += "\r\n";
//             code += "sys.path = libPaths\r\n";
//             code += "sys.dont_write_bytecode = True\r\n";
// 
//             eval( code.toStdString().c_str(), args );
        }


        template<typename T> 
        PyObject* mapToPythonDict( const T& map )
        {
            PyObject* rpDict = PyDict_New();
            Q_ASSERT( rpDict != NULLPTR );


            return rpDict;
//             auto pDict = PyDict_New();
// 
//             foreach( auto key, map.keys() )
//             {
//                 PyDict_SetItem(
//                     pDict,
//                     PyUnicode_FromStringAndSize( key.toUtf8(), key.length() ),
//                     variantToPythonObject( map.value( key ) )
//                 );
//             }
// 
//             return pDict;
        }

        QVariant ConvertToQVariant( PyObject* object )
        {
            QVariant vtData;

            do 
            {
                if( object == NULLPTR )
                    break;

                _typeobject* type = Py_TYPE( object );

                if( type == &PyBool_Type )
                {
                    // bool
                    vtData = object == Py_True ? QVariant( true ) : QVariant( false );
                }
                else if( type == &PyInt_Type )
                {
                    // int
                    vtData = QVariant( (qint64)PyInt_AsSsize_t( object ) );
                }
                else if( type == &PyLong_Type )
                {
                    // long
                    vtData = QVariant( (qlonglong)PyInt_AsLong( object ) );
                }
                else if( type == &PyFloat_Type )
                {
                    // float
                    vtData = QVariant( PyFloat_AsDouble( object ) );
                }
                else if( type == &PyString_Type )
                {
                    // string
                    vtData = QVariant( QString::fromLatin1( PyString_AsString( object ), PyString_Size( object ) ) );
                }
                else if( type == &PyUnicode_Type )
                {
                    // unicode string
                    PyObject* pStr = PyUnicode_AsUTF8String( object );

                    QString str;

                    if( pStr )
                    {
                        str = QString::fromUtf8( PyString_AsString( pStr ), PyString_Size( pStr ) );
                        Py_DECREF( pStr );
                    }

                    vtData = QVariant( str );
                }
                else if( type == &PyByteArray_Type )
                {
                    // byte array
                    vtData = QVariant( QByteArray( PyByteArray_AsString( object ), PyByteArray_Size( object ) ) );
                }
                else if( type == &PyTuple_Type )
                {
                    // tuple
                    auto size = PyTuple_Size( object );
                    auto list = QVariantList();

                    for( int i = 0; i < size; ++i )
                    {
                        list.insert( i, ConvertToQVariant( PyTuple_GetItem( object, i ) ) );
                    }

                    vtData = QVariant( list );
                }
                else if( type == &PyList_Type )
                {
                    // list
                    auto size = PyList_Size( object );
                    auto list = QVariantList();

                    for( int i = 0; i < size; ++i )
                    {
                        list.insert( i, ConvertToQVariant( PyList_GetItem( object, i ) ) );
                    }

                    vtData = QVariant( list );
                }
                else if( type == &PyDict_Type )
                {
                    // dictionary
                    auto map = QVariantMap();
                    auto pKeys = PyDict_Keys( object );
                    auto size = PyList_Size( pKeys );

                    for( int i = 0; i < size; ++i )
                    {
                        auto pKey = PyList_GetItem( pKeys, i );

                        map.insert(
                            ConvertToQVariant( pKey ).toString(),
                            ConvertToQVariant( PyDict_GetItem( object, pKey ) )
                        );
                    }

                    Py_DECREF( pKeys );

                    vtData = QVariant( map );
                }

            } while (false);

            return vtData;
        }

        QString ConvertToQString( PyObject* object )
        {
            auto pString = PyObject_Str( object );

            if( pString )
            {
                auto result = ConvertToQVariant( pString ).toString();

                Py_DECREF( pString );

                return result;
            }
            else
            {
                return QString();
            }

        }

        CPythonException::CPythonException()
        {
            _isErrorOccured = PyErr_Occurred() != NULLPTR;

            if( _isErrorOccured == true )
            {
                PyObject *pType, *pValue, *pTraceback;

                // fetch python exception
                PyErr_Fetch( &pType, &pValue, &pTraceback );
                PyErr_NormalizeException( &pType, &pValue, &pTraceback );

                // get type name
                auto pTypeName = PyObject_GetAttrString( pType, "__name__" );
                if( pTypeName )
                {
                    _type = ConvertToQString( pTypeName );
                }
                else
                {
                    _type = QString();
                }
                Py_DECREF( pType );

                // get message
                if( pValue )
                {
                    _message = ConvertToQString( pValue );
                    Py_DECREF( pValue );
                }
                else
                {
                    _message = QString();
                }

//                 // get trace
//                 QString trace;
//                 if( pTraceback )
//                 {
//                     auto pFormatTbArgs = PyTuple_New( 1 );
//                     PyTuple_SetItem( pFormatTbArgs, 0, pTraceback ); // steals reference to pTraceback
//                     trace = doCallModuleFunction( "traceback", "format_tb", pFormatTbArgs, Print ).toStringList().join( "" );
//                     Py_DECREF( pFormatTbArgs );
//                 }
//                 else
//                 {
//                     trace = QString();
//                 }

            }

        }

        CPyMain::CPyMain( const char* szProgramName /*= "CPyMain" */ )
            : _isOwnInitialized(0)
            , _isOwnThreadInitialized(0)
            , _pyMainThreadState(NULLPTR)
        {
            _isOwnInitialized = Py_IsInitialized();
            if( _isOwnInitialized == 0 )
                Py_Initialize();

            _isOwnThreadInitialized = PyEval_ThreadsInitialized();
            if( _isOwnThreadInitialized == 0 )
            {
                PyEval_InitThreads();

                _pyMainThreadState = PyEval_SaveThread();
                Q_ASSERT( _pyMainThreadState != NULLPTR );
            }
        }

        CPyMain::~CPyMain()
        {
            if( _pyMainThreadState != NULLPTR )
            {
                PyEval_RestoreThread( _pyMainThreadState );
                _pyMainThreadState = NULLPTR;
            }

            Py_Finalize();
        }

        void CPyMain::AppendPath( const QString& sSysPath )
        {
            CPyGILStateRAII gilState;

            PyRun_SimpleString( "import sys" );
            PyRun_SimpleString( QString( "sys.path.append(\"%1\")" ).arg( sSysPath ).toUtf8().data() );
        }

        nsCmn::nsPython::CPyModule CPyMain::ImportModuleFromFile( const QString& sFilePath )
        {
            QFile fFile( sFilePath );

            do 
            {
                if( fFile.open( QIODevice::ReadOnly | QIODevice::Text ) == false )
                    break;

                CPyGILStateRAII gilState;

                CPyObjectPtr spCodeObject( Py_CompileString( fFile.readAll().data(), "", Py_file_input ) );
                if( spCodeObject == NULLPTR )
                    break;

                QFileInfo fileInfo( fFile );

                // TODO: TRACE, 
                return CPyModule( PyImport_ExecCodeModule( fileInfo.baseName().toUtf8().data(), spCodeObject ) );

            } while (false);

            return CPyModule();
        }


        CPyModule::CPyModule( PyObject* object, bool isBorrowed /*= false */ )
            : CPyObjectPtr( object, isBorrowed )
        {

        }

        CPyModule::CPyModule( const CPyObjectPtr& clsObjectPtr )
            : CPyObjectPtr( clsObjectPtr )
        {

        }

        nsCmn::nsPython::CPyObjectPtr CPyModule::GetAttribute( const QString& sAttrName )
        {
            CPyGILStateRAII gilState;

            return CPyObjectPtr( PyObject_GetAttrString( _pyObject, sAttrName.toUtf8().data() ) );
        }

        QVariant CPyModule::GetVariable( const QString& sAttrName )
        {
            CPyGILStateRAII gilState;

            return ConvertToQVariant( GetAttribute( sAttrName ) );
        }

    } // nsPython
} // nsCmn
