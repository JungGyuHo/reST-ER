﻿#ifndef __HDR_UNIQUE_POLICY_DEFS__
#define __HDR_UNIQUE_POLICY_DEFS__

#include <QtCore>

struct POLICY
{
    // 분류
    QString                                         sCategory;
    // 정책 이름
    QString                                         sName;

    // 기본값
    QVariant                                        vtDefaultValue;

    // 정책값
    QVariant                                        vtValue;
};

const POLICY SPOL_CMN_SPHINX_PATH                   = { "common", "sphinxPath", "%PYTHONPATH%"  };

const POLICY SPOL_CDE_FONTNAME                      = { "code", "fontName", "D2Coding"};
const POLICY SPOL_CDE_FONTSIZE                      = { "code", "fontSize", 13 };
const POLICY SPOL_CDE_BACKGROUND_COLOR              = { "code", "backgroundColor", "black" };
const POLICY SPOL_CDE_FOREGROUND_COLOR              = { "code", "foregroundColor", "gray" };

#endif // __HDR_UNIQUE_POLICY_DEFS__