﻿#ifndef __HDR_CMN_PYTHON_UTILS__
#define __HDR_CMN_PYTHON_UTILS__

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif

#include "cmnBase.hpp"
#include "cmnConcurrent.hpp"

/*!
    파이썬
        단일 인터프리터 모드
            Py_Initialize 에 의해 생성된 기본 인터프리터를 사용한다. 
            PyGILState_* 함수를 이용하여 GIL 락을 획득/해제한다. 
                PyGILState_* 함수는 다중 인터프리터 모드에서 사용할 수 없다. 

        다중 인터프리터 모드
            Py_NewInterpreter 에 의해 독립된 추가 인터프리터를 생성하여 사용한다. 


        파이썬 초기화
            Py_Initialize
                다중 인터프리터
                Py_InitThreads, GIL G
                PyEval_SaveThread, GIL R

                PyEval_RestoreThread, GIL G
            Py_Finialize
*/

#include <QAtomicInt>

namespace nsCmn
{
    namespace nsPython
    {
        namespace nsDetail
        {
            extern QAtomicInteger<bool>             IsThreadSupport;
            extern QAtomicInteger<bool>             IsThreadSupportInitialized;

            extern QAtomicInteger<bool>             IsUseMultiInterpreter;
        } // nsDetail

        /*!
            단일 인터프리터 모드를 사용할 때 GIL 락을 획득/해제
        */
        class CPyGILStateRAII
        {
        public:
            CPyGILStateRAII() { _state = PyGILState_Ensure(); }
            ~CPyGILStateRAII() { PyGILState_Release( _state ); }

        private:
            PyGILState_STATE                        _state;
        };

        
        class CPythonModule
        {

        public:
            CPythonModule( const char* szProgamName = "", const QStringList& lstLibsPaths = QStringList() << "/libs" );
            ~CPythonModule();


            void eval( const char* szCode, const QVariantMap& mapVtLocalParams = QVariantMap(), const char* szFileNameInBackTrace = __FILE__ );


            QVariant                                GetVariable( const QString& sVarName );
            QVariant                                GetVariable( PyObject* object, const QString& sVarName );

        private:
            void                                            initializePythonEnvironment( const QStringList &libPaths );

            PyObject*                               _pyModule;

            int                                     _isOwnInitialized;
            int                                     _isOwnThreadInitialized;

            PyThreadState*                          _rpThreadState;
        };

        class CPythonException
        {
        public:
            CPythonException();

        private:

            QString                                 _type;
            QString                                 _message;
            QString                                 _traceback;

            bool                                    _isErrorOccured;
        };

        //////////////////////////////////////////////////////////////////////////

        // QVariantMap, QVariantHash, QMap, QHash
        // key 에는 toUtf8, length 메소드가 있어야한다. 
        template<typename T> 
        PyObject* mapToPythonDict( const T& map );

        QVariant ConvertToQVariant( PyObject* object );
        QString  ConvertToQString( PyObject* object );

        /*!
            PyObject* 에 대한 관리 클래스
                파이썬의 객체 참조는 새로운 참조와 빌린 참조의 두 가지 경우가 있음

                빌린참조 : 별도의 작업 필요하지 않음
                새로운 참조 : 사용이 끝난 후 참조 횟수를 감소시켜야함
        */
        class CPyObjectPtr
        {
        public:
            // PyObject* 에 대한 소유권을 이전받는다. object 에 대해 별도로 DecRef 를 호출하면 안됨
            CPyObjectPtr( PyObject* object, bool isBorrowed = false )
                : _pyObject( object ), _isBorrowed( isBorrowed )
            {}
            CPyObjectPtr( const CPyObjectPtr& clsObjectPtr )
                : _pyObject( clsObjectPtr._pyObject ), _isBorrowed( clsObjectPtr._isBorrowed )
            {
                if( _isBorrowed == false )
                    Py_IncRef( _pyObject );
            }
            ~CPyObjectPtr()
            {
                cleanUP();
            }

            operator PyObject*() { return _pyObject; }
            CPyObjectPtr& operator=( const CPyObjectPtr& clsObjectPtr )
            {
                cleanUP();

                _pyObject = clsObjectPtr._pyObject;
                _isBorrowed = clsObjectPtr._isBorrowed;

                if( _isBorrowed == false )
                    Py_IncRef( _pyObject );
            }
            
            bool operator ==( void* rsh ) { return _pyObject == rsh; }

        protected:
            void                                    cleanUP()
            {
                if( _pyObject != NULLPTR && _isBorrowed == false )
                    Py_DecRef( _pyObject );

                _pyObject = NULLPTR;
                _isBorrowed = false;
            }

            bool                                    _isBorrowed;
            PyObject*                               _pyObject;
        };

        class CPyModule;

        /*!
            파이썬 메인 인터프리터 선언
        */
        class CPyMain
        {
        public:
            CPyMain( const char* szProgramName = "CPyMain" );
            ~CPyMain();

            //////////////////////////////////////////////////////////////////////////
            /// 기본 SYS 모듈 관련

            void                                    SetPath( const QStringList& lstSysPath );
            void                                    AppendPath( const QString& sSysPath );
            QVariantMap                             Modules();

            //////////////////////////////////////////////////////////////////////////
            ///

            void                                    Eval( const QString& sCode, const QVariantMap& vtMapLocal );

            //////////////////////////////////////////////////////////////////////////
            /// 모듈 관련

            // 경로 에서 모듈을 찾아 불러온다. 
            CPyModule                               ImportModule( const QString& sModuleName );
            CPyModule                               ImportModuleFromCode( const QString& sCode );
            CPyModule                               ImportModuleFromFile( const QString& sFilePath );

        private:

            int                                     _isOwnInitialized;
            int                                     _isOwnThreadInitialized;

            PyThreadState*                          _pyMainThreadState;
        };

        class CPyModule : public CPyObjectPtr
        {
        public:
            CPyModule( PyObject* object = NULLPTR, bool isBorrowed = false );
            CPyModule( const CPyObjectPtr& clsObjectPtr );

            // 변수 가져오기 / 설정하기
            // 함수 호출하기
            CPyObjectPtr                            GetAttribute( const QString& sAttrName );

            QVariant                                GetVariable( const QString& sAttrName );
            QVariant                                ObjectToVariant( CPyObjectPtr spObjectPtr );


        private:
        };

        class CPyInterpreter
        {
        public:
            CPyInterpreter()
                : _pyThreadState( NULLPTR )
            {
                // get the GIL
                PyEval_AcquireLock();   
                _pyThreadState = Py_NewInterpreter();
                if( _pyThreadState == NULL )
                    ;
                PyEval_ReleaseThread( _pyThreadState );
            }
            ~CPyInterpreter()
            {
                // get the GIL
                PyEval_AcquireThread( _pyThreadState );
                Py_EndInterpreter( _pyThreadState );
                // release the GIL
                PyEval_ReleaseLock();
            }

            void Lock() { PyEval_AcquireThread( _pyThreadState ); }
            void Unlock() { PyEval_ReleaseThread( _pyThreadState ); }

        private:
            PyThreadState*                          _pyThreadState;
        };

        typedef QSharedPointer< CPyInterpreter >    TySpPyInterpreter;

        class CPyInterpreterLocker
        {
        public:

            CPyInterpreterLocker( CPyInterpreter* rpInterpreter )
                : _pyInterpreter( rpInterpreter )
            {
                _pyInterpreter->Lock();
            }
            ~CPyInterpreterLocker()
            {
                _pyInterpreter->Unlock();
            }

        private:
            CPyInterpreter*                         _pyInterpreter;
        };

    } // nsPython
} // nsCmn

#define LOCK_GIL_OR_INTERPRETER \
nsCmn::nsPython::nsDetail::IsUseMultiInterpreter.loadAcquire();


#endif // __HDR_CMN_PYTHON_UTILS__