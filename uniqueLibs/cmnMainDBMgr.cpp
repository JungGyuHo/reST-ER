﻿#include "stdafx.h"
#include "cmnMainDBMgr.hpp"

#include "cmnTypeDefs_Names.hpp"

using namespace nsCmn;
using namespace nsCmn::nsCmnDB;

//////////////////////////////////////////////////////////////////////////

bool CMainDBMgr::Init()
{
    bool isSuccess = false;

    do 
    {
        auto stDBMgr = TyStCmnDBMgr::GetInstance();
        
        TyDatabaseOptions dbOptions;

        dbOptions.sDBIdentity = DBMS_MAINDB_IDEN;
        dbOptions.sDBName = APP_MAINDB_FILENAME;
        dbOptions.sDBType = "QSQLITE";
        dbOptions.sSchemaFileName = DBMS_MAINDB_SCHEMA;

        isSuccess = stDBMgr->InitDBMS( dbOptions );

    } while (false);

    return isSuccess;
}

QVector< POLICY > CMainDBMgr::GetPolicies()
{
    QVector< POLICY > vecPolicies;
    auto stCmnDBMgr = TyStCmnDBMgr::GetInstance();
    auto db = stCmnDBMgr->GetConnection( DBMS_MAINDB_IDEN );
    CCmnDBTransactionGuard< CCmnDBRecursiveTransaction > dbTransactionMgr( db );

    do 
    {
        QSqlQuery dbStmt( *db );

        

    } while (false);

    return vecPolicies;
}

bool CMainDBMgr::SetPolicies( const QVector< POLICY >& vecPolicies )
{
    auto stCmnDBMgr = TyStCmnDBMgr::GetInstance();
    auto db = stCmnDBMgr->GetConnection( DBMS_MAINDB_IDEN );
    CCmnDBTransactionGuard< CCmnDBRecursiveTransaction > dbTransactionMgr( db );

    do 
    {
        QSqlQuery dbStmt( *db );

        QString sQuery = "INSERT OR REPLACE INTO TBL_SETTINGS ( CATEGORY, NAME, VALUE ) VALUES ( :CATEGORY, :NAME, :VALUE )";
        db->Prepare( dbStmt, sQuery );

        for( auto item : vecPolicies )
        {
            dbStmt.bindValue( ":CATEGORY", item.sCategory );
            dbStmt.bindValue( ":NAME", item.sName );
            if( item.vtValue.isValid() == false || item.vtValue.isNull() == true )
                dbStmt.bindValue( ":VALUE", item.vtDefaultValue  );
            else
                dbStmt.bindValue( ":VALUE", item.vtValue );
            IF_FALSE_BREAK( dbTransactionMgr, db->ExecuteSQL( dbStmt ) );
            dbStmt.finish();
        }

    } while (false);

    return dbTransactionMgr.apply();
}
