﻿#ifndef __HDR_UNIQUE_TYPEDEF_NAMES__
#define __HDR_UNIQUE_TYPEDEF_NAMES__

#include <QObject>
#include <QString>

const QString APP_MAINDB_FILENAME = "main.db";

const QString APP_PROJECTDB_FILEEXT = "reST";

const QString APP_MAIN_TITLE_UI = QObject::tr( "reST-ER" );

const QString SPHINX_CONF_FILENAME = "conf.py";

#endif // __HDR_UNIQUE_TYPEDEF_NAMES__