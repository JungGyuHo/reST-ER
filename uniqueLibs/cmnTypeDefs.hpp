﻿#ifndef __HDR_UNIQUE_TYPEDEF__
#define __HDR_UNIQUE_TYPEDEF__

#include <QString>

//////////////////////////////////////////////////////////////////////////
/// Solution Tree

const int SOLUTIONTREE_ITEM_TYPE_ROLE = Qt::UserRole + 1;
enum TyEnSolutionTreeItemType { SolutionTreeItemTop, SolutionTreeItemDir, SolutionTreeItemFile };

Q_DECLARE_METATYPE( TyEnSolutionTreeItemType );

#endif // __HDR_UNIQUE_TYPEDEF__