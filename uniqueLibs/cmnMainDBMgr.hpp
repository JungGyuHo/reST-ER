﻿#ifndef __HDR_CMN_MAINDB_MGR__
#define __HDR_CMN_MAINDB_MGR__

#include "cmnDB.hpp"
#include "cmnConcurrent.hpp"

#include "cmnPolicyDefs.hpp"

const QString DBMS_MAINDB_IDEN = "mainDB-Identity";
const QString DBMS_MAINDB_SCHEMA = ":/dbms/Resources/reST-ER-App.xml";

class CMainDBMgr
{
public:

    bool                                            Init();


    QVector< POLICY >                               GetPolicies();
    bool                                            SetPolicies( const QVector< POLICY >& vecPolicies );

private:

};

typedef nsCmn::nsConcurrent::TSingleton< CMainDBMgr >       TyStMainDBMgr;

#endif // __HDR_CMN_MAINDB_MGR__